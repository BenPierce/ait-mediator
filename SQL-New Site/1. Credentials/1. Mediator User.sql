CASE WHEN NOT EXISTS (Select FIRST 1 from SYS.SYSUSER u WHERE u.user_name = 'Mediator') THEN
	GRANT CONNECT TO "Mediator" IDENTIFIED BY  'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
	comment on user "Mediator" is 'NZ Mediator Web Application';
	GRANT manage ANY web service TO Mediator;
END
GO

CASE WHEN NOT EXISTS (Select FIRST 1 from SYSGROUPS where member_name = 'Mediator' and group_name = 'Manager') THEN
    GRANT MEMBERSHIP IN GROUP Manager to Mediator;
END
GO
