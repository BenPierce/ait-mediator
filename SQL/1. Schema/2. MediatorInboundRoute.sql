IF OBJECT_ID('Administrator.MediatorInboundRoute') IS NULL 
BEGIN
	CREATE TABLE Administrator.MediatorInboundRoute
	(
		RouteId	INTEGER primary KEY DEFAULT autoincrement,
		Name VARCHAR(255) NOT NULL,
		UrlMask LONG VARCHAR NOT NULL,
		"Sql" LONG VARCHAR NOT null,
		DeserializeRequest BIT DEFAULT 0,
		SerializeResponse BIT DEFAULT 0,
		IsDocumentRequest BIT DEFAULT 0
	);
	
	CREATE UNIQUE INDEX MediatorInboundRoute_Name_Idx ON Administrator.MediatorInboundRoute(Name);
END
GO

