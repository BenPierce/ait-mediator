IF OBJECT_ID('Administrator.MediatorApp') IS NULL 
BEGIN
	CREATE TABLE Administrator.MediatorApp
	(
		AppId			INTEGER primary KEY DEFAULT autoincrement,
		Name 			VARCHAR(255) NOT NULL,
		AppKeyEnc 		LONG VARCHAR NOT null,
		IsDisabled 		BIT DEFAULT 0
	);
	
	CREATE UNIQUE INDEX MediatorApp_Name_Idx ON Administrator.MediatorApp(Name);
	CREATE UNIQUE INDEX MediatorApp_AppKeyEnc_Idx ON Administrator.MediatorApp(AppKeyEnc);
END
GO
