IF OBJECT_ID('Administrator.MediatorOutboundRoute') IS NULL 
BEGIN
	CREATE TABLE Administrator.MediatorOutboundRoute
	(
		RouteId	INTEGER primary KEY DEFAULT autoincrement,
		Name VARCHAR(255) NOT NULL,		
		RequestType INTEGER NOT NULL, --0 Post
		RequestUrlSql LONG varchar NOT null,
		RequestHeadersSql LONG varchar DEFAULT null,
		DeserializeRequestHeaders BIT DEFAULT 0,
		RequestBodySql LONG VARCHAR DEFAULT null,
		SerializeRequestbody BIT DEFAULT 0,
		ResponseSql LONG VARCHAR DEFAULT null,
		ResponseErrorSql LONG VARCHAR DEFAULT NULL,
		DeserializeResponse BIT DEFAULT 0,
		ProceedSql LONG VARCHAR DEFAULT null
	);
	CREATE UNIQUE INDEX MediatorOutboundRoute_Name_Idx ON Administrator.MediatorOutboundRoute(Name);
	CREATE CLUSTERED INDEX MediatorOutboundRoute_RouteId_Idx ON Administrator.MediatorOutboundRoute(RouteId);
END
GO
