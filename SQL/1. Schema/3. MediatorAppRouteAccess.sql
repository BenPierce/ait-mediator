IF OBJECT_ID('Administrator.MediatorAppRouteAccess') IS NULL 
BEGIN
	CREATE TABLE Administrator.MediatorAppRouteAccess
	(
		AccessId INTEGER primary KEY DEFAULT autoincrement,
		AppId INTEGER NOT NULL DEFAULT NULL,
		RouteId INTEGER NOT NULL DEFAULT NULL
	);
	ALTER TABLE Administrator.MediatorAppRouteAccess ADD CONSTRAINT MediatorAppRouteAccess_App_FK FOREIGN KEY (AppId) REFERENCES Administrator.MediatorApp(AppId);
	ALTER TABLE Administrator.MediatorAppRouteAccess ADD CONSTRAINT MediatorAppRouteAccess_Route_FK FOREIGN KEY (RouteId) REFERENCES Administrator.MediatorInboundRoute(RouteId);
	
	CREATE UNIQUE INDEX MediatorAppRouteAccess_App_Route ON Administrator.MediatorAppRouteAccess(AppId, RouteId);
END
GO

