CASE WHEN NOT EXISTS (Select FIRST 1 from SYS.SYSCOLUMNS c WHERE c.tname = 'MediatorInboundRoute' AND c.cname = 'IsDocumentRequest') THEN
	ALTER TABLE Administrator.MediatorInboundRoute ADD IsDocumentRequest BIT DEFAULT 0;
END
GO

