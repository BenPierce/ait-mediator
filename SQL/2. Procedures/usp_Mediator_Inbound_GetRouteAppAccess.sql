CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Inbound_GetRouteAppAccess
(
	IN in_RouteId integer
)
BEGIN
	Select 
		a.AppId			AS AppId,
		in_RouteId		AS RouteId,
		ra.AccessId		AS AccessId,
		a.name			AS AppName,
		CASE WHEN ra.AppId IS NULL THEN 0 ELSE 1 END
						AS AccessGranted,
		a.IsDisabled	AS IsAppDisabled				
	from MediatorApp a
		LEFT JOIN MediatorAppRouteAccess ra ON a.AppId = ra.AppId AND ra.RouteId = in_RouteId
end
GO


