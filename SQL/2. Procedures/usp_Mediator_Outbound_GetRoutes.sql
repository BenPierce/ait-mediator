CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Outbound_GetRoutes
(
	IN in_RouteId INTEGER DEFAULT null
)
BEGIN

	SELECT 
		o.RouteId,
		o.Name,
		o.RequestType,
		o.RequestUrlSql,
		o.RequestHeadersSql,
		o.DeserializeRequestHeaders,
		o.RequestBodySql,
		o.SerializeRequestbody,
		o.ResponseSql,
		o.ResponseErrorSql,
		o.DeserializeResponse,
		o.ProceedSql
	FROM MediatorOutboundRoute o
	WHERE 1=1
		AND (o.RouteId = in_RouteId OR  in_RouteId = NULL)
end
GO
