CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Inbound_DeleteRouteAppAccess
(
	IN in_RouteId INTEGER
)
BEGIN
	DELETE MediatorAppRouteAccess
	from MediatorAppRouteAccess ra
	WHERE 1=1
		AND ra.RouteId = in_RouteId;
end
GO


