CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Inbound_GetApp
(
	IN in_AppId integer
)
RESULT
(
	AppId	integer, 
	Name	varchar(255),
	AppKeyEnc	long varchar,
	IsDisabled	bit
)
BEGIN
	SELECT 
		a.AppId,
		a.Name,
		a.AppKeyEnc,
		a.IsDisabled	
	FROM Administrator.MediatorApp a
	WHERE 1=1
		AND a.AppId  = in_AppId;
end
GO


