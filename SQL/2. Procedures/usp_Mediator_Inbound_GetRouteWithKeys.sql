CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Inbound_GetRouteWithKeys()
BEGIN
	SELECT
		a.AppKeyEnc, 
		m.RouteId,
		m.Name,
		m.UrlMask,
		m."Sql",
		m.DeserializeRequest,
		m.SerializeResponse,
		m.IsDocumentRequest
	FROM Administrator.MediatorInboundRoute m
		INNER JOIN Administrator.MediatorAppRouteAccess ra ON m.RouteId = ra.RouteId
			INNER JOIN Administrator.MediatorApp a ON ra.AppId = a.AppId
	WHERE 1=1
		AND a.IsDisabled = 0
end
GO
