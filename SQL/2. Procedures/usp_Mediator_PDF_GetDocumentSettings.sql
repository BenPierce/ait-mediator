CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_PDF_GetDocumentSettings
(
	IN in_DocumentNo	VARCHAR(255),
	IN in_DocumentCode	CHAR(3)
)
BEGIN
	 DECLARE const_EnvironmentKey VARCHAR(100) = 'Mediator_EnvironmentName';
	 DECLARE const_OutputDirectoryKey VARCHAR(100) = 'Mediator_OutputDirectory';
	 DECLARE const_ReportDirectoryKey VARCHAR(100) = 'Mediator_ReportDirectory';
	 
	 DECLARE var_DocumentNo	VARCHAR(255) = TRIM(ISNULL(in_DocumentNo,''));	 
	 DECLARE var_TemplateDirectory LONG VARCHAR;
	 
	 DECLARE out_EnvironmentName VARCHAR(255);	 
	 DECLARE out_OutputFilePath LONG VARCHAR;
	 DECLARE out_TemplateFilePath LONG VARCHAR;
	 DECLARE out_ParameterTypes LONG VARCHAR;
	 DECLARE out_ParameterValues LONG VARCHAR;
	 

	 CASE WHEN NOT EXISTS (Select FIRST 1 FROM syconfig s WHERE s.sys_key = const_EnvironmentKey) THEN
	 	INSERT INTO syconfig WITH auto name
	 		Select
	 			const_EnvironmentKey						AS sys_key,
	 			ISNULL((Select FIRST s.value_string from syconfig s WHERE s.sys_key = 'Enable Environment Name'),'Production')						
	 														AS value_string;	 		
	 END;	
	 
	 SET out_EnvironmentName = (Select FIRST s.value_string from syconfig s WHERE s.sys_key = const_EnvironmentKey);
	 
	 CASE WHEN NOT EXISTS (Select FIRST 1 FROM syconfig s WHERE s.sys_key = const_OutputDirectoryKey) THEN
	 	INSERT INTO syconfig WITH auto name
	 		Select
	 			const_OutputDirectoryKey					AS sys_key,
	 			xp_getenv('AITHOST') || 'AITData\Mediator\'	AS value_string;	 		
	 END;	
	 
	 --SET out_OutputFilePath = (Select FIRST s.value_string from syconfig s WHERE s.sys_key = const_OutputDirectoryKey) || var_DocumentNo || '-' || NEWID() || '.pdf';
	 SET out_OutputFilePath = (Select FIRST s.value_string from syconfig s WHERE s.sys_key = const_OutputDirectoryKey);
	 
	 
	 CASE WHEN NOT EXISTS (Select FIRST 1 FROM syconfig s WHERE s.sys_key = const_ReportDirectoryKey) THEN
	  	SET var_TemplateDirectory =  xp_getenv('AITHOST') || 'AITINSTALL\REPORTS\';
	 ELSE
	 	SET var_TemplateDirectory = (Select FIRST s.value_string from syconfig s WHERE s.sys_key = const_ReportDirectoryKey);
		CASE WHEN RIGHT(var_TemplateDirectory,1) <> '\' THEN SET var_TemplateDirectory = var_TemplateDirectory || '\' END;	 		
	 END;
	 
	 CASE 
	 	WHEN in_DocumentCode = 'PUR' THEN
	 		CASE WHEN NOT EXISTS (Select FIRST 1 FROM Invoice i WHERE i.invo_type = 'S' and i.document_no = var_DocumentNo) THEN 
	 			Select 1 AS IsError, 'Document No "' || in_DocumentNo || '" does not exist'	AS ErrorMessage;
	 			RETURN;
	 		END;
	 		
	 		SET out_TemplateFilePath = (Select FIRST var_TemplateDirectory || i.layout_name FROM Invoice i WHERE i.invo_type = 'S' and i.document_no = var_DocumentNo);
	 		SET out_ParameterTypes = 'string~string';
	 		SET out_ParameterValues = var_DocumentNo || '~' || 'Y';
	 	ELSE 
	 		Select 1 AS IsError, 'Document Code "' || in_DocumentCode || '" not supported'	AS ErrorMessage;
	 		RETURN;
	 END; 
	
	
	Select
		out_EnvironmentName     AS EnvironmentName,
		out_OutputFilePath      AS OutputFilePath,
		out_TemplateFilePath	AS TemplateFilePath,
		out_ParameterTypes  	AS ParameterTypes,
		out_ParameterValues		AS ParameterValues;
		
end
GO


