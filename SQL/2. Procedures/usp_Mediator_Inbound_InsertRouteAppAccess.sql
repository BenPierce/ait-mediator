CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Inbound_InsertRouteAppAccess
(
	IN in_RouteId INTEGER,
	IN in_AppId INTEGER
)
BEGIN
	
	INSERT INTO MediatorAppRouteAccess WITH auto name
		Select 
			in_RouteId 	AS RouteId,
			in_AppId	AS AppId;
	
end
GO
