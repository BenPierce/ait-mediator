CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Inbound_DeleteApp
(
	IN in_AppId INTEGER
)
BEGIN
	DELETE MediatorAppRouteAccess
	from MediatorAppRouteAccess ra
	WHERE 1=1
		AND ra.AppId = in_AppId;
		
	DELETE MediatorApp
	from MediatorApp a
	WHERE 1=1
		AND a.AppId = in_AppId;
end
GO
