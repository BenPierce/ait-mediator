CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Outbound_UpsertRoute
(
	IN in_RouteId						INTEGER,
	IN in_Name							VARCHAR(255),
	IN in_RequestType					INTEGER,
	IN in_RequestUrlSql					LONG VARCHAR,
	IN in_RequestHeadersSql				LONG VARCHAR,
	IN in_DeserializeRequestHeaders		BIT,
	IN in_RequestBodySql				LONG VARCHAR,
	IN in_SerializeRequestBody			BIT,
	IN in_ResponseSql					LONG VARCHAR,
	IN in_ResponseErrorSql				LONG VARCHAR,
	IN in_DeserializeResponse			bit,
	IN in_ProceedSql					LONG VARCHAR
)
BEGIN
	IF in_RouteId is NULL THEN
		BEGIN
			INSERT INTO Administrator.MediatorOutboundRoute WITH auto name
				SELECT 
					in_RouteId					 as	RouteId,
					in_Name						 as	Name,
					in_RequestType				 as	RequestType,
					in_RequestUrlSql			 as	RequestUrlSql,
					in_RequestHeadersSql		 as	RequestHeadersSql,
					in_DeserializeRequestHeaders as	DeserializeRequestHeaders,
					in_RequestBodySql			 as	RequestBodySql,
					in_SerializeRequestBody		 as	SerializeRequestbody,
					in_ResponseSql				 as	ResponseSql,
					in_ResponseErrorSql			 AS ResponseErrorSql,
					in_DeserializeResponse		 as	DeserializeResponse,
					in_ProceedSql				 AS ProceedSql;

			CALL usp_Mediator_Outbound_GetRoutes(@@IDENTITY);											

		END
	ELSE
		BEGIN
			INSERT INTO Administrator.MediatorOutboundRoute ON EXISTING UPDATE WITH auto name
				SELECT 
					in_RouteId					 as	RouteId,
					in_Name						 as	Name,
					in_RequestType				 as	RequestType,
					in_RequestUrlSql			 as	RequestUrlSql,
					in_RequestHeadersSql		 as	RequestHeadersSql,
					in_DeserializeRequestHeaders as	DeserializeRequestHeaders,
					in_RequestBodySql			 as	RequestBodySql,
					in_SerializeRequestBody		 as	SerializeRequestbody,
					in_ResponseSql				 as	ResponseSql,
					in_ResponseErrorSql			 AS ResponseErrorSql,
					in_DeserializeResponse		 as	DeserializeResponse,
					in_ProceedSql				 AS ProceedSql;
			
			CALL usp_Mediator_Outbound_GetRoutes(in_RouteId);								
		END
	END if
	
end
GO
