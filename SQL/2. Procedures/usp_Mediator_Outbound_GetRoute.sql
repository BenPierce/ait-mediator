CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Outbound_GetRoute
(
	IN in_RouteId integer
)
BEGIN
	SELECT first
		RouteId,
		Name,
		RequestType,
		RequestUrlSql,
		RequestHeadersSql,
		DeserializeRequestHeaders,
		RequestBodySql,
		SerializeRequestbody,
		ResponseSql,
		ResponseErrorSql,
		DeserializeResponse,
		ProceedSql
	FROM Administrator.MediatorOutboundRoute m
	WHERE 1=1
		AND m.RouteId = in_RouteId
end
go
