CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Inbound_UpsertRoute
(
	IN in_RouteId INTEGER,
	IN in_Name VARCHAR(255),
	IN in_UrlMask LONG VARCHAR,
	IN in_Sql LONG VARCHAR,
	IN in_DeserializeRequest BIT,
	IN in_SerializeResponse BIT,
	IN in_IsDocumentRequest BIT
)
RESULT
(
	out_RouteId integer
)
BEGIN
	IF in_RouteId is NULL THEN
		BEGIN
			INSERT INTO Administrator.MediatorInboundRoute WITH auto name
				SELECT 
					in_Name              	AS Name,
					in_UrlMask           	AS UrlMask,
					in_Sql               	AS "SQL",
					in_DeserializeRequest	AS DeserializeRequest,
					in_SerializeResponse 	AS SerializeResponse,
					in_IsDocumentRequest	AS IsDocumentRequest;
					
			select @@IDENTITY;								

		END
	ELSE
		BEGIN
			INSERT INTO Administrator.MediatorInboundRoute ON EXISTING UPDATE WITH auto name
				SELECT 
					in_RouteId				AS RouteId,
					in_Name              	AS Name,
					in_UrlMask           	AS UrlMask,
					in_Sql               	AS "SQL",
					in_DeserializeRequest	AS DeserializeRequest,
					in_SerializeResponse 	AS SerializeResponse,
					in_IsDocumentRequest	AS IsDocumentRequest;
			
			select in_RouteId;								
		END
	END if
	
end
GO
