CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Outbound_DeleteRoute
(
	IN in_RouteId INTEGER
)
BEGIN
		
	DELETE MediatorOutboundRoute
	from MediatorOutboundRoute r
	WHERE 1=1
		AND r.RouteId = in_RouteId;
end
GO
