CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Inbound_GetApps()
BEGIN
	SELECT 
		a.AppId,
		a.Name,
		a.AppKeyEnc,
		a.IsDisabled	
	FROM Administrator.MediatorApp a
end
GO
