CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Inbound_UpsertApp
(
	IN in_AppId INTEGER,
	IN in_Name VARCHAR(255),
	IN in_AppKeyEnc LONG VARCHAR,
	IN in_IsDisabled bit
)
BEGIN
	IF in_AppId is NULL THEN
		BEGIN
			INSERT INTO Administrator.MediatorApp WITH auto name
				SELECT 
					in_Name              	AS Name,
					in_AppKeyEnc            AS AppKeyEnc,
					in_IsDisabled			AS IsDisabled;
					
			Select * from usp_Mediator_Inbound_GetApp(@@IDENTITY);								

		END
	ELSE
		BEGIN
			INSERT INTO Administrator.MediatorApp ON EXISTING UPDATE WITH auto name
				SELECT 
					in_AppId				AS AppId,
					in_Name              	AS Name,
					in_AppKeyEnc            AS AppKeyEnc,
					in_IsDisabled			AS IsDisabled;
			
			Select * from usp_Mediator_Inbound_GetApp(in_AppId);	
		END
	END if
	
end
GO
