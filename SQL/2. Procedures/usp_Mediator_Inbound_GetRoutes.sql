CREATE OR REPLACE PROCEDURE administrator.usp_Mediator_Inbound_GetRoutes
(
	IN in_RouteId INTEGER DEFAULT null
)
BEGIN

	DROP TABLE if EXISTS #RoutesIGR;
	CREATE TABLE #RoutesIGR
	(
		RouteId             INTEGER NOT NULL,
		Name                VARCHAR(255) NOT NULL,
		UrlMask             LONG VARCHAR NOT NULL,
		SQL                 LONG VARCHAR NOT NULL,
		DeserializeRequest  BIT NOT NULL,
		SerializeResponse   BIT NOT NULL,
		IsDocumentRequest 	BIT NOT NULL
	);
	
	INSERT INTO #RoutesIGR WITH auto name
		SELECT 
			m.RouteId,
			m.Name,
			m.UrlMask,
			m."Sql",
			m.DeserializeRequest,
			m.SerializeResponse,
			m.IsDocumentRequest
		FROM Administrator.MediatorInboundRoute m
		WHERE 1=1
			AND (m.RouteId = in_RouteId OR in_RouteId IS null);
			
			
	DROP TABLE if EXISTS #RouteAccessIGR;
	CREATE TABLE #RouteAccessIGR
	(
		AppId			INTEGER not null,
		RouteId			INTEGER not null,
		AccessId		INTEGER DEFAULT null,
		AppName			char(255) not null,
		IsAccessGranted	BIT NOT null,
		IsAppDisabled	BIT NOT null
	); 
	
	INSERT INTO #RouteAccessIGR WITH auto name
		Select 
			a.AppId			AS AppId,
			i.RouteId		AS RouteId,
			ra.AccessId		AS AccessId,
			a.name			AS AppName,
			CASE WHEN ra.AppId IS NULL THEN 0 ELSE 1 END
							AS IsAccessGranted,
			a.IsDisabled	AS IsAppDisabled				
		from MediatorInboundRoute i
			LEFT JOIN MediatorApp a ON 1=1
				left join MediatorAppRouteAccess ra on a.AppId = ra.AppId AND ra.RouteId = i.RouteId

		WHERE 1=1
			AND (i.RouteId = in_RouteId OR in_RouteId is NULL);

	DROP TABLE if EXISTS #OutputIGR;
	CREATE TABLE #OutputIGR
	(
		Tag											INTEGER,
		Parent										INTEGER,
		[!1!RouteId]                                INTEGER,     
		[!1!Name]                                   VARCHAR(255),
		[!1!UrlMask]                                LONG VARCHAR,
		[!1!Sql]                                  LONG VARCHAR,
		[!1!DeserializeRequest]                     BIT,         
		[!1!SerializeResponse]                      BIT, 
		[!1!IsDocumentRequest]                      BIT, 
		
		[AppAccessDMs!2!AppId]			    INTEGER,    
		[AppAccessDMs!2!AccessId]		    INTEGER,
		[AppAccessDMs!2!AppName]			char(255),
		[AppAccessDMs!2!IsAccessGranted]	    BIT,        
		[AppAccessDMs!2!IsAppDisabled]		BIT         	
	);
	
	INSERT INTO #OutputIGR WITH auto name
		Select
			1						AS Tag,
			null					AS Parent,
			r.RouteId               AS [!1!RouteId],
			r.Name                  AS [!1!Name],
			r.UrlMask               AS [!1!UrlMask],
			r."Sql"                 AS [!1!Sql],
			r.DeserializeRequest    AS [!1!DeserializeRequest],
			r.SerializeResponse     AS [!1!SerializeResponse],
			r.IsDocumentRequest     AS [!1!IsDocumentRequest],
					 
			NULL					AS [AppAccessDMs!2!AppId],
			NULL					AS [AppAccessDMs!2!AccessId],
			NULL					AS [AppAccessDMs!2!AppName],
			0						AS [AppAccessDMs!2!IsAccessGranted],
			0						AS [AppAccessDMs!2!IsAppDisabled]
		FROM #RoutesIGR r;
				
	INSERT INTO #OutputIGR WITH auto name
		Select
			2						AS Tag,
			1						AS Parent,
			ra.RouteId              AS [!1!RouteId],
			NULL				    AS [!1!Name],
			NULL				    AS [!1!UrlMask],
			NULL				    AS [!1!Sql],
			0					    AS [!1!DeserializeRequest],
			0					    AS [!1!SerializeResponse],
			0						AS [!1!IsDocumentRequest],
					 
			ra.AppId        		AS [AppAccessDMs!2!AppId],
			ra.AccessId     		AS [AppAccessDMs!2!AccessId],
			ra.AppName      		AS [AppAccessDMs!2!AppName],
			ra.IsAccessGranted		AS [AppAccessDMs!2!IsAccessGranted],
			ra.IsAppDisabled		AS [AppAccessDMs!2!IsAppDisabled]
		FROM #RouteAccessIGR ra;			
				
				
	Select 
		* 
	from #OutputIGR
	ORDER by
		[!1!RouteId],
		[AppAccessDMs!2!AppId]
	FOR JSON explicit;		

							
end
GO
