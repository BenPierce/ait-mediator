--TABLES
EXECUTE Immediate '' || (Select list('Grant Select,insert,update,delete on administrator.' || t.table_name || ' to Mediator',CHAR(10) || CHAR(13)) from SYS.SYSTABLE t WHERE t.table_name LIKE 'Mediator_%' AND t.table_type = 'BASE')
GO



-- PROCEDURES
EXECUTE Immediate '' || (Select list('Grant execute on ' || p.proc_name || ' to Mediator ',CHAR(10) || CHAR(13)) AS SQL FROM SYS.SYSPROCEDURE p WHERE p.proc_name LIKE 'usp_mediator%')
GO


