﻿namespace AIT.Mediator.Common.PDF.Abstractions;

public interface IPdfDocument
{
    string DocumentNo { get; }
    string DocumentCode { get; }
    string DocumentType { get; }
}

