﻿namespace AIT.Mediator.Common.PDF.Abstractions;

public interface IPdfDocumentSettings
{
    string EnvironmentName { get; }
    string OutputFilePath { get; }    
    string TemplateFilePath { get; }
    string ParameterTypes { get; }
    string ParameterValues { get; }

    public string[] ParameterTypesAry => ParameterTypes?.ToLower().Split('~').ToArray();
    public string[] ParameterValuesAry => ParameterValues?.Split('~').ToArray();
}
