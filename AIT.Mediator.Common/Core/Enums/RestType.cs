﻿namespace AIT.Mediator.Common.Core.Enums;

public enum RestType
{
    INVALID = 0,
    POST = 1,
    GET = 2,
    PUT = 3,
    PATCH = 4,
    DELETE = 5
}

public static class RestTypeExtensions
{
    public static HttpMethod ToHttpMethod(this RestType type)
    {
        return type switch
        {
            RestType.POST => HttpMethod.Post,
            RestType.GET => HttpMethod.Get,
            RestType.PUT => HttpMethod.Put,
            RestType.PATCH => HttpMethod.Patch,
            RestType.DELETE => HttpMethod.Delete,
            _ => throw new Exception($"Unsupported {nameof(RestType)}: {type}")
        };
    }

    public static string ToFriendly(this RestType type)
    {
        return type switch
        {
            RestType.INVALID => @"",
            RestType.POST => @"Post",
            RestType.GET => @"Get",
            RestType.PUT => @"Put",
            RestType.PATCH => @"Patch",
            RestType.DELETE => @"Delete",
            _ => throw new Exception($"Unsupported {nameof(RestType)}: {type}")
        };
    }
}
