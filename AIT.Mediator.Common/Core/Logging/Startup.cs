﻿using AIT.IOC;
using AIT.Mediator.Common.Core.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Exceptions;

namespace AIT.Mediator.Common.Core.Logging;

public static class Startup
{
    public static void AddLogging(this IServiceCollection services, string appName)
    {
        if (string.IsNullOrWhiteSpace(appName)) throw new Exception(nameof(appName));

        services.AddConfigurationSingleton<LoggingConfiguration>("Logging");

        var loggingConfiguration = services.BuildServiceProvider().GetRequiredService<LoggingConfiguration>();

        Log.Logger = new LoggerConfiguration()
            .Enrich.FromLogContext()
            .Enrich.WithExceptionDetails()
            .MinimumLevel.Is(loggingConfiguration.Default)
            .MinimumLevel.Override("Microsoft", loggingConfiguration.Microsoft)
            .MinimumLevel.Override("System", loggingConfiguration.System)
            .MinimumLevel.Override("AIT.Database", loggingConfiguration.Database)
            .WriteTo.Console()
            .WriteTo.File(Path.Combine("log", $"{appName}-.log"),
                              fileSizeLimitBytes: 1024 * 1024 * 1024,
                              retainedFileCountLimit: 30,
                              rollingInterval: RollingInterval.Day,
                              outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level}] {Message}{NewLine}{Exception}")
            .CreateLogger();

        services.AddLogging(loggingBuilder =>
        {
            loggingBuilder.ClearProviders();
            loggingBuilder.AddSerilog();
        });
    }
}
