﻿using Serilog.Events;

namespace AIT.Mediator.Common.Core.Logging;

class LoggingConfiguration
{
    public LogEventLevel Default { get; set; }
    public LogEventLevel Microsoft { get; set; }
    public LogEventLevel System { get; set; }
    public LogEventLevel Database { get; set; }
}
