﻿using Microsoft.Extensions.Primitives;

namespace AIT.Mediator.Common.Core.Extensions;

public static class StringValuesExtensions
{
    public static string ToSingleString(this StringValues values)
    {
        if (values.Any() != true)
        {
            return null;
        }
        else
        {
            var output = values.Select(s => s ?? "").ToDelim(",");

            return string.IsNullOrWhiteSpace(output)
                ? null
                : output;
        }
    }
}
