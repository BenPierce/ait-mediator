﻿using System.Text;

namespace AIT.Mediator.Common.Core.Extensions;

public static class StringExtensions
{
    public static string Truncate(this string value, int maxLength)
    {
        if (string.IsNullOrWhiteSpace(value)) return value;
        return value.Length <= maxLength ? value : value.Substring(0, maxLength);
    }

    public static bool IsSame(this string value, string other)
    {
        if (string.IsNullOrWhiteSpace(value) && string.IsNullOrWhiteSpace(other))
        {
            return true;
        }
        else
        {
            return value?.Equals(other, StringComparison.OrdinalIgnoreCase) ?? false;
        }
    }

    public static bool SameAs(this string value, string other) => value.IsSame(other);

    public static string ToDelim(this IEnumerable<string> strings, string delimiter = " ")
    {
        var stringBuilder = new StringBuilder();

        return string.Join(delimiter, strings.ToArray());
    }

    public static Stream ToStream(this string value)
    {
        try
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(value);
            writer.Flush();
            stream.Position = 0;

            return stream;
        }
        catch (Exception ex)
        {
            throw new Exception($"Failed to convert string to stream: {ex.Message}", ex);
        }
    }
}
