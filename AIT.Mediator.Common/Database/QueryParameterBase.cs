﻿namespace AIT.Mediator.Common.Database;

public abstract class QueryParameterBase
{
    public string Set(
        string value,
        string fieldName,
        bool isNullable = true,
        int? maxLength = null,
        bool isUpperCase = false
        )
    {
        if (string.IsNullOrWhiteSpace(value))
        {
            if (isNullable == false)
            {
                throw new Exception($"Parameter field {fieldName} can not be null");
            }
            else
            {
                return null;
            }
        }
        else
        {
            if (maxLength != null && value.Length > maxLength)
            {
                throw new Exception($"Parameter field {fieldName} can not be more then {maxLength} characters");
            }
            else
            {
                if (isUpperCase) value = value.ToUpper();

                return value?.Trim();
            }
        }
    }

    public int Set(
        int value,
        string fieldName,
        bool isZeroAllowed = true,
        bool isNegativeAllowed = true
        )
    {
        if (isZeroAllowed == false && value == 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be zero");
        }
        else if (isNegativeAllowed == false && value < 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be negative");
        }
        else
        {
            return value;
        }
    }

    public int? Set(
        int? value,
        string fieldName,
        bool isZeroAllowed = true,
        bool isNegativeAllowed = true
        )
    {
        if (isZeroAllowed == false && value == 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be zero");
        }
        else if (isNegativeAllowed == false && value < 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be negative");
        }
        else
        {
            return value;
        }
    }

    public static DateTime Set(
        DateTime value,
        string fieldName,
        bool isAllowedDefault = true,
        int? maxDaysAhead = null,
        bool isTimeAllowed = true
        )
    {
        if (isAllowedDefault == false && value == default)
        {
            throw new Exception($"Parameter field {fieldName} can not an empty value");
        }
        else if (maxDaysAhead != null & (value.Date - DateTime.Today).TotalDays > (int)maxDaysAhead)
        {
            throw new Exception($"Parameter field {fieldName} must not be more then {maxDaysAhead} days in the future");
        }
        else
        {
            if (isTimeAllowed)
            {
                return value;
            }
            else
            {
                return value.Date;
            }

        }
    }

    public static Guid Set(
        Guid value,
        string fieldName,
        bool isAllowedDefault = true
        )
    {
        if (isAllowedDefault == false && value == default)
        {
            throw new Exception($"Parameter field {fieldName} can not an empty value");
        }
        else
        {
            return value;
        }
    }

    public static bool Set(
        bool value,
        string fieldName
        )
    {
        return value;
    }
}
