﻿namespace AIT.Mediator.Common.Mapping.Abstractions.Services;

public interface IMapperService
{
    TTo Map<TTo>(object obj);
    TTo Map<TFrom, TTo>(TFrom obj);
    object Map(object obj, Type typeFrom, Type typeTo);
}
