﻿namespace AIT.Mediator.Common.Communication;

public interface IUdpService
{
    Task NotifyChange();
}
