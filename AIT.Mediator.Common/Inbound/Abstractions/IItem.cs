﻿namespace AIT.Mediator.Common.Inbound.Abstractions;

public interface IItem
{
    int? Id { get; }
}
