﻿namespace AIT.Mediator.Common.Inbound.Abstractions;

public interface IInboundRouteAppAccess
{
    int? AccessId { get; }
    int AppId { get; }
    string AppName { get; }
    bool IsAccessGranted { get; }
    bool IsAppDisabled { get; }
}
