﻿namespace AIT.Mediator.Common.Inbound.Abstractions;

public interface IApp : IItem
{
    int? AppId { get; }
    string Name { get; }
    string AppKeyEnc { get; }
    bool IsDisabled { get; }
}
