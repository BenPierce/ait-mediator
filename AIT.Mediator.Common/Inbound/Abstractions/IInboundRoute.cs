﻿using System.Text.RegularExpressions;

namespace AIT.Mediator.Common.Inbound.Abstractions;

public partial interface IInboundRoute : IItem
{
    int? RouteId { get; }
    string Name { get; }
    string UrlMask { get; }
    string Sql { get; }
    bool DeserializeRequest { get; }
    bool SerializeResponse { get; }
    bool IsDocumentRequest { get; }
    
    IEnumerable<IInboundRouteAppAccess> AppAccess { get; }

    public string Mask => string.IsNullOrWhiteSpace(UrlMask)
                        ? null
                        : Regex.Replace(UrlMask, @"{.*?}", m => String.Format("*", m.Value));

    public int MaskVariableCount => Regex.Matches(UrlMask, @"{.*?}")?.Count() ?? 0;

    public bool IsMaskMatch(string url)
    {
        var mask = Mask;

        var maskRegex = new Regex($"^{mask.Replace("*", ".*")}$", RegexOptions.IgnoreCase);

        return maskRegex.IsMatch(url);
    }
}       
