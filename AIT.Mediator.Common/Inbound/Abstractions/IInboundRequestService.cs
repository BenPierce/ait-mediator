﻿using Microsoft.AspNetCore.Http;

namespace AIT.Mediator.Common.Inbound.Abstractions;

public interface IInboundRequestService : IDisposable
{
    Task ProcessRequest(HttpContext context);
}
