﻿using AIT.Crypto;

namespace AIT.Mediator.Common.Inbound.Abstractions;

public partial interface IInboundRouteWithKeys : IInboundRoute
{
    string AppKeyEnc { get; }

    public bool IsValidAppKey(string appkey) => appkey?.Equals(AppKeyEnc?.Decrypt()) == true;

}       
