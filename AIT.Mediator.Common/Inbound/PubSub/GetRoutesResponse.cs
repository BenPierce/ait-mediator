﻿using AIT.Mediator.Common.Inbound.Abstractions;

namespace AIT.Mediator.Common.Inbound.PubSub;

public class GetRoutesResponse
{
    public IEnumerable<IInboundRouteWithKeys> RouteWithKeys { get; set; }
}
