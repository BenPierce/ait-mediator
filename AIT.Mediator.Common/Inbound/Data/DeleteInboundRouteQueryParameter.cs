﻿using AIT.Mediator.Common.Database;

namespace AIT.Mediator.Common.Inbound.Data;

public class DeleteInboundRouteQueryParameter : QueryParameterBase
{
    public int in_RouteId { get; private set; }

    public DeleteInboundRouteQueryParameter(
        int? routeId
        )
    {
        if (routeId == null) throw new Exception("Cannot delete route with null RouteId");

        in_RouteId = Set(
           value: (int)routeId,
           fieldName: nameof(routeId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

    }
}
