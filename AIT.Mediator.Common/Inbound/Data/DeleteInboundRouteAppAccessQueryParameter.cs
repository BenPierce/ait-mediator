﻿using AIT.Mediator.Common.Database;

namespace AIT.Mediator.Common.Inbound.Data;

public class DeleteInboundRouteAppAccessQueryParameter : QueryParameterBase
{
    public int in_RouteId { get; private set; }

    public DeleteInboundRouteAppAccessQueryParameter(
        int routeId
        )
    { 
        in_RouteId = Set(
           value: routeId,
           fieldName: nameof(routeId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

    }
}
