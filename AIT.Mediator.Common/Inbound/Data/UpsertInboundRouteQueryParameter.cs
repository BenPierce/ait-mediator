﻿using AIT.Mediator.Common.Database;
using AIT.Mediator.Common.Inbound.Abstractions;

namespace AIT.Mediator.Common.Inbound.Data;

public class UpsertInboundRouteQueryParameter : QueryParameterBase
{
    public int? in_RouteId { get; private set; }
    public string in_Name { get; private set; }
    public string in_UrlMask { get; private set; }
    public string in_Sql { get; private set; }
    public bool in_DeserializeRequest { get; private set; }
    public bool in_SerializeResponse { get; private set; }

    public UpsertInboundRouteQueryParameter(
        int? routeId,
        string name,
        string urlMask,
        string sql,
        bool deserializeRequest,
        bool serializeResponse
        )
    {
        in_RouteId = Set(
           value: routeId,
           fieldName: nameof(routeId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

        in_Name = Set(
            value: name,
            fieldName: nameof(name),
            isNullable: false,
            maxLength: 255,
            isUpperCase: false
            );

        in_UrlMask = Set(
            value: urlMask,
            fieldName: nameof(urlMask),
            isNullable: false,
            maxLength: null,
            isUpperCase: false
            );

        in_Sql = Set(
            value: sql,
            fieldName: nameof(sql),
            isNullable: false,
            maxLength: null,
            isUpperCase: false
            );

        in_DeserializeRequest = Set(
            value: deserializeRequest,
            fieldName: nameof(deserializeRequest)
            );

        in_SerializeResponse = Set(
            value: serializeResponse,
            fieldName: nameof(serializeResponse)
            );
    }

    public UpsertInboundRouteQueryParameter(
        IInboundRoute route
        )
    {
        in_RouteId = Set(
           value: route.RouteId,
           fieldName: nameof(route.RouteId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

        in_Name = Set(
            value: route.Name,
            fieldName: nameof(route.Name),
            isNullable: false,
            maxLength: 255,
            isUpperCase: false
            );

        in_UrlMask = Set(
            value: route.UrlMask,
            fieldName: nameof(route.UrlMask),
            isNullable: false,
            maxLength: null,
            isUpperCase: false
            );

        in_Sql = Set(
            value: route.Sql,
            fieldName: nameof(route.Sql),
            isNullable: false,
            maxLength: null,
            isUpperCase: false
            );

        in_DeserializeRequest = Set(
            value: route.DeserializeRequest,
            fieldName: nameof(route.DeserializeRequest)
            );

        in_SerializeResponse = Set(
            value: route.SerializeResponse,
            fieldName: nameof(route.SerializeResponse)
            );
    }
}
