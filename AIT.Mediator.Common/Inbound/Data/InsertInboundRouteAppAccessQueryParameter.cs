﻿using AIT.Mediator.Common.Database;

namespace AIT.Mediator.Common.Inbound.Data;

public class InsertInboundRouteAppAccessQueryParameter : QueryParameterBase
{
    public int in_RouteId { get; private set; }
    public int in_AppId { get; private set; }

    public InsertInboundRouteAppAccessQueryParameter(
        int routeId,
        int appId
        )
    {
        in_RouteId = Set(
           value: routeId,
           fieldName: nameof(routeId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

        in_AppId = Set(
           value: appId,
           fieldName: nameof(appId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );
    }
}
