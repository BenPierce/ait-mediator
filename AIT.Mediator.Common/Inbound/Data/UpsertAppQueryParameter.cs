﻿using AIT.Mediator.Common.Database;
using AIT.Mediator.Common.Inbound.Abstractions;

namespace AIT.Mediator.Common.Inbound.Data;

public class UpsertAppQueryParameter : QueryParameterBase
{
    public int? in_AppId { get; private set; }
    public string in_Name { get; private set; }
    public string in_AppKeyEnc { get; private set; }
    public bool in_IsDisabled { get; private set; }

    public UpsertAppQueryParameter(
        int? appId,
        string name,
        string appKeyEnc,
        bool isDisabled
        )
    {
        in_AppId = Set(
           value: appId,
           fieldName: nameof(appId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

        in_Name = Set(
            value: name,
            fieldName: nameof(name),
            isNullable: false,
            maxLength: 255,
            isUpperCase: false
            );

        in_AppKeyEnc = Set(
            value: appKeyEnc,
            fieldName: nameof(appKeyEnc),
            isNullable: false,
            maxLength: null,
            isUpperCase: false
            );

        in_IsDisabled = Set(
            value: isDisabled,
            fieldName: nameof(isDisabled)
            );
    }

    public UpsertAppQueryParameter(
        IApp app
        )
    {
        in_AppId = Set(
           value: app.AppId,
           fieldName: nameof(app.AppId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

        in_Name = Set(
            value: app.Name,
            fieldName: nameof(app.Name),
            isNullable: false,
            maxLength: 255,
            isUpperCase: false
            );

        in_AppKeyEnc = Set(
            value: app.AppKeyEnc,
            fieldName: nameof(app.AppKeyEnc),
            isNullable: false,
            maxLength: null,
            isUpperCase: false
            );

        in_IsDisabled = Set(
            value: app.IsDisabled,
            fieldName: nameof(app.IsDisabled)
            );
    }
}
