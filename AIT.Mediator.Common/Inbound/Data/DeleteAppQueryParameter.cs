﻿using AIT.Mediator.Common.Database;

namespace AIT.Mediator.Common.Inbound.Data;

public class DeleteAppQueryParameter : QueryParameterBase
{
    public int in_AppId { get; private set; }

    public DeleteAppQueryParameter(
        int? appId
        )
    {
        if (appId == null) throw new Exception("Cannot delete app with null AppId");

        in_AppId = Set(
           value: (int)appId,
           fieldName: nameof(appId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

    }
}
