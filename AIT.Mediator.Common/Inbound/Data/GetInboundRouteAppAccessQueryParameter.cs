﻿using AIT.Mediator.Common.Database;

namespace AIT.Mediator.Common.Inbound.Data;

public class GetInboundRouteAppAccessQueryParameter : QueryParameterBase
{
    public int in_RouteId { get; private set; }

    public GetInboundRouteAppAccessQueryParameter(
        int routeId
        )
    {
        in_RouteId = Set(
           value: routeId,
           fieldName: nameof(routeId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );
    }
}
