﻿using AIT.Mediator.Common.Database;

namespace AIT.Mediator.Common.Inbound.Data;

public class GetInboundRouteQueryParameter : QueryParameterBase
{
    public int in_RouteId { get; private set; }

    public GetInboundRouteQueryParameter(
        int routeId
        )
    {
        in_RouteId = Set(
           value: routeId,
           fieldName: nameof(routeId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );
    }
}
