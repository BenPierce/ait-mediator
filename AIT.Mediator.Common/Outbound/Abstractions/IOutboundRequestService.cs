﻿namespace AIT.Mediator.Common.Outbound.Abstractions;

public interface IOutboundRequestService
{
    Task RequestById(int routeId, IEnumerable<string> parameters);
}
