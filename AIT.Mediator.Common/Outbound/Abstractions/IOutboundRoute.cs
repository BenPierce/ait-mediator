﻿using AIT.Mediator.Common.Core.Enums;
using AIT.Mediator.Common.Inbound.Abstractions;

namespace AIT.Mediator.Common.Outbound.Abstractions;

public interface IOutboundRoute : IItem
{
    int? RouteId { get; }
    string Name { get; }
    RestType RequestType { get; }
    string RequestUrlSql { get; }
    string RequestHeadersSql { get; }
    bool DeserializeRequestHeaders { get; }
    string RequestBodySql { get;}
    bool SerializeRequestBody { get; }
    string ResponseSql { get; }
    string ResponseErrorSql { get; }
    bool DeserializeResponse { get; }
    string ProceedSql { get; }
}
