﻿using AIT.Mediator.Common.Outbound.Data;

namespace AIT.Mediator.Common.Outbound.Abstractions;

public interface IOutboundDataSource
{
    Task<IOutboundRoute> GetRoute(GetOutboundRouteQueryParameter parameter);
    Task<bool> GetProceed(string proceedSql, VariableQueryParameter parameter);
    Task<string> GetRouteUrl(string outboundUrlSql, VariableQueryParameter parameter);        
    Task<string> GetHeadersSerialized(string outboundHeadersSql, VariableQueryParameter parameter);
    Task<IDictionary<string,string>> GetHeaderUnserialized(string outboundHeadersSql, VariableQueryParameter parameter);
    Task<string> GetBodySerialized(string outboundBodySql, VariableQueryParameter parameter);
    Task<IEnumerable<dynamic>> GetBodyUnserialized(string outboundBodySql, VariableQueryParameter parameter);
    Task ExecuteRouteResponse(string query, IDictionary<string, object> parameters);

    Task<IEnumerable<IOutboundRoute>> GetOutboundRoutes();
    Task<IOutboundRoute> UpsertOutboundRoute(UpsertOutboundRouteQueryParameter parameter);
    Task DeleteOutboundRoute(DeleteOutboundRouteQueryParameter parameter);
}
