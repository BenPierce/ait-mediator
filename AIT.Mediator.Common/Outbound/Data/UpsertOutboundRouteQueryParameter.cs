﻿using AIT.Mediator.Common.Core.Enums;
using AIT.Mediator.Common.Database;
using AIT.Mediator.Common.Outbound.Abstractions;

namespace AIT.Mediator.Common.Outbound.Data;

public class UpsertOutboundRouteQueryParameter : QueryParameterBase
{
    public int? in_RouteId { get; init; }
    public string in_Name { get; init; }
    public int in_RequestType { get; init; }
    public string in_RequestUrlSql { get; init; }
    public string in_RequestHeadersSql { get; init; }
    public bool in_DeserializeRequestHeaders { get; init; }
    public string in_RequestBodySql { get; init; }
    public bool in_SerializeRequestBody { get; init; }
    public string in_ResponseSql { get; init; }
    public string in_ResponseErrorSql { get; init; }
    public bool in_DeserializeResponse { get; init; }
    public string in_ProceedSql { get; init; }

    public UpsertOutboundRouteQueryParameter(
        int? routeId,
        string name,
        RestType RequestType,
        string requestUrlSql,
        string requestHeadersSql,
        bool deserializeRequestHeaders,
        string requestBodySql,
        bool serializeRequestBody,
        string responseSql,
        string responseErrorSql,
        bool deserializeResponse,
        string proceedSql
        )
    {
        in_RouteId = Set(
           value: routeId,
           fieldName: nameof(routeId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

        in_Name = Set(
            value: name,
            fieldName: nameof(name),
            isNullable: false,
            maxLength: 255,
            isUpperCase: false
            );

        in_RequestType = Set(
            value: (int)RequestType,
            fieldName: nameof(RequestType),
            isZeroAllowed: false,
            isNegativeAllowed: false
            );

        in_RequestUrlSql = Set(
            value: requestUrlSql,
            fieldName: nameof(requestUrlSql),
            isNullable: false,
            maxLength: null,
            isUpperCase: false
            );

        in_RequestHeadersSql = Set(
            value: requestHeadersSql,
            fieldName: nameof(requestHeadersSql),
            isNullable: true,
            maxLength: null,
            isUpperCase: false
            );

        in_DeserializeRequestHeaders = Set(
            value: deserializeRequestHeaders,
            fieldName: nameof(deserializeRequestHeaders)
            );

        in_RequestBodySql = Set(
            value: requestBodySql,
            fieldName: nameof(requestBodySql),
            isNullable: true,
            maxLength: null,
            isUpperCase: false
            );

        in_SerializeRequestBody = Set(
           value: serializeRequestBody,
           fieldName: nameof(serializeRequestBody)
           );

        in_ResponseSql = Set(
            value: responseSql,
            fieldName: nameof(responseSql),
            isNullable: true,
            maxLength: null,
            isUpperCase: false
            );

        in_ResponseErrorSql = Set(
           value: responseErrorSql,
           fieldName: nameof(responseErrorSql),
           isNullable: true,
           maxLength: null,
           isUpperCase: false
           );

        in_DeserializeResponse = Set(
           value: deserializeResponse,
           fieldName: nameof(deserializeResponse)
           );

        in_ProceedSql = Set(
            value: proceedSql,
            fieldName: nameof(proceedSql),
            isNullable: true,
            maxLength: null,
            isUpperCase: false
            );
    }

    public UpsertOutboundRouteQueryParameter(
        IOutboundRoute outboundRoute
        )
    {
        in_RouteId = Set(
           value: outboundRoute.RouteId,
           fieldName: nameof(outboundRoute.RouteId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

        in_Name = Set(
            value: outboundRoute.Name,
            fieldName: nameof(outboundRoute.Name),
            isNullable: false,
            maxLength: 255,
            isUpperCase: false
            );

        in_RequestType = Set(
            value: (int)outboundRoute.RequestType,
            fieldName: nameof(outboundRoute.RequestType),
            isZeroAllowed: false,
            isNegativeAllowed: false
            );

        in_RequestUrlSql = Set(
            value: outboundRoute.RequestUrlSql,
            fieldName: nameof(outboundRoute.RequestUrlSql),
            isNullable: false,
            maxLength: null,
            isUpperCase: false
            );

        in_RequestHeadersSql = Set(
            value: outboundRoute.RequestHeadersSql,
            fieldName: nameof(outboundRoute.RequestHeadersSql),
            isNullable: true,
            maxLength: null,
            isUpperCase: false
            );

        in_DeserializeRequestHeaders = Set(
            value: outboundRoute.DeserializeRequestHeaders,
            fieldName: nameof(outboundRoute.DeserializeRequestHeaders)
            );

        in_RequestBodySql = Set(
            value: outboundRoute.RequestBodySql,
            fieldName: nameof(outboundRoute.RequestBodySql),
            isNullable: true,
            maxLength: null,
            isUpperCase: false
            );

        in_SerializeRequestBody = Set(
           value: outboundRoute.SerializeRequestBody,
           fieldName: nameof(outboundRoute.SerializeRequestBody)
           );

        in_ResponseSql = Set(
            value: outboundRoute.ResponseSql,
            fieldName: nameof(outboundRoute.ResponseSql),
            isNullable: true,
            maxLength: null,
            isUpperCase: false
            );

        in_ResponseErrorSql = Set(
           value: outboundRoute.ResponseErrorSql,
           fieldName: nameof(outboundRoute.ResponseErrorSql),
           isNullable: true,
           maxLength: null,
           isUpperCase: false
           );

        in_DeserializeResponse = Set(
           value: outboundRoute.DeserializeResponse,
           fieldName: nameof(outboundRoute.DeserializeResponse)
           );

        in_ProceedSql = Set(
            value: outboundRoute.ProceedSql,
            fieldName: nameof(outboundRoute.ProceedSql),
            isNullable: true,
            maxLength: null,
            isUpperCase: false
            );
    }
}
