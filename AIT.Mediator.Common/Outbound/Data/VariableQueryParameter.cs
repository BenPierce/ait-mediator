﻿using AIT.Mediator.Common.Database;

namespace AIT.Mediator.Common.Outbound.Data;

public class VariableQueryParameter : QueryParameterBase
{
    public string Variable1 { get; private set; }
    public string Variable2 { get; private set; }
    public string Variable3 { get; private set; }
    public string Variable4 { get; private set; }
    public string Variable5 { get; private set; }
    public string Variable6 { get; private set; }
    public string Variable7 { get; private set; }
    public string Variable8 { get; private set; }
    public string Variable9 { get; private set; }
    public string Variable10 { get; private set; }

    public VariableQueryParameter(
        params string[] variables
        )
    {

        var variableCount = variables?.Count() ?? 0;

        if (variableCount > 10) throw new Exception("Too many variables provided");

        if (variableCount >= 1)
        {
            Variable1 = Set(
               value: variables[0],
               fieldName: "Variable1",
               isNullable: true
           );
        }

        if (variableCount >= 2)
        {
            Variable2 = Set(
               value: variables[1],
               fieldName: "Variable2",
               isNullable: true
           );
        }

        if (variableCount >= 3)
        {
            Variable3 = Set(
               value: variables[2],
               fieldName: "Variable3",
               isNullable: true
           );
        }

        if (variableCount >= 4)
        {
            Variable4 = Set(
               value: variables[3],
               fieldName: "Variable4",
               isNullable: true
           );
        }


        if (variableCount >= 5)
        {
            Variable5 = Set(
               value: variables[4],
               fieldName: "Variable5",
               isNullable: true
           );
        }

        if (variableCount >= 6)
        {
            Variable6 = Set(
               value: variables[5],
               fieldName: "Variable6",
               isNullable: true
           );
        }

        if (variableCount >= 7)
        {
            Variable7 = Set(
               value: variables[6],
               fieldName: "Variable7",
               isNullable: true
           );
        }

        if (variableCount >= 8)
        {
            Variable8 = Set(
               value: variables[7],
               fieldName: "Variable8",
               isNullable: true
           );
        }

        if (variableCount >= 9)
        {
            Variable9 = Set(
               value: variables[8],
               fieldName: "Variable9",
               isNullable: true
           );
        }

        if (variableCount == 10)
        {
            Variable10 = Set(
               value: variables[9],
               fieldName: "Variable10",
               isNullable: true
           );
        }

    }
}
