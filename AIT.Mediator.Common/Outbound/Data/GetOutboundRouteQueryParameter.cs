﻿using AIT.Mediator.Common.Database;

namespace AIT.Mediator.Common.Outbound.Data;

public class GetOutboundRouteQueryParameter : QueryParameterBase
{
    public int in_RouteId { get; private set; }

    public GetOutboundRouteQueryParameter(
        int? routeId
        )
    {
        if (routeId == null) throw new Exception("Cannot get outbound route with null RouteId");

        in_RouteId = Set(
           value: (int)routeId,
           fieldName: nameof(routeId),
           isZeroAllowed: false,
           isNegativeAllowed: false
           );

    }
}
