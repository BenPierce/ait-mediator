﻿using AIT.Crypto;
using AIT.Mediator.Common.Inbound.Abstractions;
using FluentValidation;

namespace AIT.Mediator.Editor.Apps.UI;

public class AppVM : IApp
{
    public int? AppId { get; set; }
    public string Name { get; set; }
    public string AppKeyEnc { get; set; }
    public bool IsDisabled { get; set; }

    public int? Id => AppId;

    public string AppKey
    {
        get
        {
            try
            {
                return AppKeyEnc?.Decrypt();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to decrypt app key");
                return null;
            }
        }

        set => AppKeyEnc = value?.Encrypt();
    }

    internal class AppVMValidator : AbstractValidator<AppVM>
    {
        public AppVMValidator()
        {
            RuleFor(a => a.Name)
                .NotEmpty()
                .WithMessage("Name is required");

            RuleFor(a => a.AppKeyEnc)
                .NotEmpty()
                .WithMessage("App Key is required");
        }
    }
}
