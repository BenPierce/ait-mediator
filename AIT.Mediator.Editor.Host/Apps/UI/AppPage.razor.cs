﻿using AIT.Mediator.Common.Communication;
using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.Common.Inbound.Data;
using AIT.Mediator.Common.Mapping.Abstractions.Services;
using AIT.Mediator.Database.Inbound;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;

namespace AIT.Mediator.Editor.Apps.UI;

partial class AppPage
{
    [Inject] private IInboundDataSource _inboundDataSource { get; set; }
    [Inject] private IUdpService _udpService { get; set; }
    [Inject] private IMapperService _mapperService { get; set; }

    private async Task<List<IApp>> GetAllApps()
    {
        return (await _inboundDataSource.GetApps())?.ToList() ?? new List<IApp>();
    }

    private async Task<IApp> Save(AppVM appVM)
    {
        var app = await _inboundDataSource.UpsertApp(new(
            app: appVM
            ));

        await _udpService.NotifyChange();

        _toastService.Success($"{appVM?.Name} saved successfully");

        return app;
    }

    private async Task<AppVM> Copy(IApp app)
    {
        var appVM = _mapperService.Map<AppVM>(app);

        appVM.AppId = null;
        appVM.Name = $"{appVM.Name} - Copy";

        _toastService.Success($"{appVM?.Name} copied successfully");
        return appVM;
    }

    private async Task Delete(AppVM appVM)
    {
        await _inboundDataSource.DeleteApp(new(
            appId: appVM.Id
            ));

        await _udpService.NotifyChange();

        _toastService.Success($"{appVM?.Name} deleted successfully");
    }

    private async Task<AppVM> New() => new AppVM();

    private async Task<IEnumerable<IApp>> SearchApps(IEnumerable<string> searchParts, IEnumerable<IApp> items)
    {
        try
        {
            IEnumerable<IApp> apps = items?.ToList();

            if (apps?.Any() == true && searchParts?.Any() == true)
            {
                foreach (var searchPart in searchParts)
                {
                    var working = apps.Where(app => app.Name.Contains(searchPart, StringComparison.OrdinalIgnoreCase)).ToList();

                    apps = apps.Intersect(working);
                }
            }

            return apps;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "App search error");
            await _messageService.ErrorAsync("Failed to process search", ex);

            return null;
        }
    }
}
