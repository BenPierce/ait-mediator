﻿using AIT.Mediator.Editor.Core.UI.Notifications;
using AIT.Mediator.Editor.Core.UI.Toast;
using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;

namespace AIT.Mediator.Editor.Core.UI;

public abstract class MediatorComponentBase<T> : ComponentBase
{
    [Inject]
    protected ILogger<T> _logger { get; set; }

    [Inject]
    protected IToastService _toastService { get; set; }

    [Inject]
    protected IMessageService _messageService { get; set; }

    [Inject]
    protected IModalService _modalService { get; set; }

    protected bool IsReady { get; set; }
    protected string ErrorMessage { get; set; }

    protected Task StateHasChangedThreadSafe() => InvokeAsync(StateHasChanged);

    protected void Error(Exception ex, string message, bool setErrorMessage = true)
    {
        if (setErrorMessage) ErrorMessage = message;

        _logger.LogError(ex, message);
        _messageService.Error($"{message}: {ex.Message}", ex);
    }
}
