﻿using Blazored.Modal;
using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;

namespace AIT.Mediator.Editor.Core.UI;

public abstract class MediatorModalBase<T> : MediatorComponentBase<T>
{
    [CascadingParameter]
    public BlazoredModalInstance ModalInstance { get; set; }

    protected async Task Ok<TResults>(TResults result)
    {
        var modalResult = ModalResult.Ok(result);

        await ModalInstance.CloseAsync(modalResult);
    }

    protected async Task Ok(bool isSuccess = true)
    {
        var modalResult = ModalResult.Ok(isSuccess);

        await ModalInstance.CloseAsync(modalResult);
    }

    protected async Task Close() => await Ok(false);

    protected async Task Cancel() => await ModalInstance.CancelAsync();

}
