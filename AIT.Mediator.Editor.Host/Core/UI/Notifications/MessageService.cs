﻿using Blazored.Modal;
using Blazored.Modal.Services;
using static AIT.Mediator.Editor.Core.UI.Notifications.MessageComponent;

namespace AIT.Mediator.Editor.Core.UI.Notifications;

public interface IMessageService
{
    void Success(string message, string title = null);
    void Failure(string message, string title = null);
    void Warning(string message, string title = null);
    void Error(string message, Exception exception, string title = null, bool alertSupport = false, bool alertDealerAdmin = false, string errorId = null);
    void CriticalError(string message, Exception exception, string title = null, bool alertSupport = false, bool alertDealerAdmin = false, string errorId = null);
    void Info(string message, string title = null);

    Task<bool> Question(string message, string title = null, string affirmativeLabel = "Yes", string negativeLabel = "No");

    Task SuccessAsync(string message, string title = null);
    Task FailureAsync(string message, string title = null);
    Task WarningAsync(string message, string title = null);
    Task ErrorAsync(string message, Exception exception, string title = null, bool alertSupport = false, bool alertDealerAdmin = false, string errorId = null);
    Task CriticalErrorAsync(string message, Exception exception, string title = null, bool alertSupport = false, bool alertDealerAdmin = false, string errorId = null);
    Task InfoAsync(string message, string title = null);
}

class MessageService : IMessageService
{
    private readonly IModalService _modalService;

    public MessageService(
        IModalService modalService
        )
    {
        _modalService = modalService;
    }

    private const string _alertSupportMessage = @"Please contact Auto-IT support for assistance";
    private const string _alertDealershipAdministrator = @"Please contact your dealership administrator for assistance with this issue";

    public void CriticalError(
        string message,
        Exception ex,
        string title = null,
        bool alertSupport = false,
        bool alertDealerAdmin = false,
        string errorId = null
        )
    {
        var parameters = BuildModalParameters(
            message: $"{message}{Environment.NewLine}{(alertSupport ? _alertSupportMessage : "")}{(alertDealerAdmin ? _alertDealershipAdministrator : "")}".Trim(),
            title: title ?? "Critical Error",
            MessageType: MessageType.CriticalError
            );

        parameters.Add("Exception", ex);

        _modalService.Show<MessageComponent>(nameof(CriticalError), parameters);
    }

    public async Task CriticalErrorAsync(
        string message,
        Exception ex,
        string title = null,
        bool alertSupport = false,
        bool alertDealerAdmin = false,
        string errorId = null
        )
    {
        var parameters = BuildModalParameters(
            message: $"{message}{Environment.NewLine}{(alertSupport ? _alertSupportMessage : "")}{(alertDealerAdmin ? _alertDealershipAdministrator : "")}".Trim(),
            title: title ?? "Critical Error",
            MessageType: MessageType.CriticalError
            );

        parameters.Add("Exception", ex);

        var instance = _modalService.Show<MessageComponent>(nameof(CriticalError), parameters);
        await instance.Result;
    }



    public void Error(
        string message,
        Exception ex,
        string title = null,
        bool alertSupport = false,
        bool alertDealerAdmin = false,
        string errorId = null
        )
    {

        var parameters = BuildModalParameters(
           message: $"{message}{Environment.NewLine}{(alertSupport ? _alertSupportMessage : "")}{(alertDealerAdmin ? _alertDealershipAdministrator : "")}".Trim(),
           title: title ?? "Opps...",
           MessageType: MessageType.Error
           );

        parameters.Add("Exception", ex);

        _modalService.Show<MessageComponent>(nameof(Error), parameters);
    }

    public async Task ErrorAsync(
        string message,
        Exception ex,
        string title = null,
        bool alertSupport = false,
        bool alertDealerAdmin = false,
        string errorId = null
        )
    {

        var parameters = BuildModalParameters(
           message: $"{message}{Environment.NewLine}{(alertSupport ? _alertSupportMessage : "")}{(alertDealerAdmin ? _alertDealershipAdministrator : "")}".Trim(),
           title: title ?? "Opps...",
           MessageType: MessageType.Error
           );

        parameters.Add("Exception", ex);

        var instance = _modalService.Show<MessageComponent>(nameof(Error), parameters);
        await instance.Result;
    }

    public void Failure(string message, string title = null)
    {
        var parameters = BuildModalParameters(
           message: message,
           title: title ?? "Opps...",
           MessageType: MessageType.Failure
           );

        _modalService.Show<MessageComponent>(nameof(Failure), parameters);
    }

    public async Task FailureAsync(string message, string title = null)
    {
        var parameters = BuildModalParameters(
           message: message,
           title: title ?? "Opps...",
           MessageType: MessageType.Failure
           );

        var instance = _modalService.Show<MessageComponent>(nameof(Failure), parameters);
        await instance.Result;
    }

    public void Info(string message, string title = null)
    {
        var parameters = BuildModalParameters(
           message: message,
           title: title,
           MessageType: MessageType.Info
           );

        _modalService.Show<MessageComponent>(nameof(Info), parameters);
    }

    public async Task InfoAsync(string message, string title = null)
    {
        var parameters = BuildModalParameters(
           message: message,
           title: title,
           MessageType: MessageType.Info
           );

        var instance = _modalService.Show<MessageComponent>(nameof(Info), parameters);
        await instance.Result;
    }

    public void Success(string message, string title = null)
    {
        var parameters = BuildModalParameters(
            message: message,
            title: title ?? "Horray!",
            MessageType: MessageType.Success
            );

        _modalService.Show<MessageComponent>(nameof(Success), parameters);
    }

    public async Task SuccessAsync(string message, string title = null)
    {
        var parameters = BuildModalParameters(
            message: message,
            title: title ?? "Horray!",
            MessageType: MessageType.Success
            );

        var instance = _modalService.Show<MessageComponent>(nameof(Success), parameters);
        await instance.Result;
    }

    public void Warning(string message, string title = null)
    {
        var parameters = BuildModalParameters(
            message: message,
            title: title ?? @"Um...",
            MessageType: MessageType.Warning
            );

        _modalService.Show<MessageComponent>(nameof(Warning), parameters);
    }

    public async Task WarningAsync(string message, string title = null)
    {
        var parameters = BuildModalParameters(
            message: message,
            title: title ?? "Um...",
            MessageType: MessageType.Warning
            );

        var instance = _modalService.Show<MessageComponent>(nameof(Warning), parameters);
        await instance.Result;
    }

    private ModalParameters BuildModalParameters(
        string message,
        string title,
        MessageType MessageType,
        string errorId = null)
    {
        var parameters = new ModalParameters();

        parameters.Add("Message", message);
        parameters.Add("Title", title);
        parameters.Add("Type", MessageType);
        parameters.Add("ErrorId", errorId);

        return parameters;
    }

    public async Task<bool> Question(string message, string title = null, string affirmativeLabel = "Yes", string negativeLabel = "No")
    {
        var parameters = new ModalParameters();

        parameters.Add("Message", message);
        parameters.Add("Title", title);
        parameters.Add("Type", MessageType.Question);
        parameters.Add("AffirmativeLabel", affirmativeLabel);
        parameters.Add("NegativeLabel", negativeLabel);

        var modalReference = _modalService.Show<MessageComponent>(title ?? "Confirm", parameters);

        var result = await modalReference.Result;
        if (result?.Cancelled != true && result?.Data?.GetType() == typeof(bool))
        {
            return (bool)result.Data;
        }
        else
        {
            return false;
        }
    }
}
