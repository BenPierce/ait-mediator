﻿using Blazored.Modal;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace AIT.Mediator.Editor.Core.UI.Notifications;

partial class MessageComponent
{
    [CascadingParameter]
    public BlazoredModalInstance _modalInstance { get; set; }

    [Parameter]
    public MessageType Type { get; set; }

    [Parameter]
    public string Title { get; set; }

    [Parameter]
    public string Message { get; set; }

    [Parameter]
    public string AffirmativeLabel { get; set; } = "Yes";

    [Parameter]
    public string NegativeLabel { get; set; } = "No";

    [Parameter]
    public Exception Exception { get; set; }

    [Parameter]
    public string ErrorId { get; set; }

    [Inject]
    private IJSRuntime _jsRuntime { get; set; }

    private bool _expandErrorDetail { get; set; }

    private const string _id = @"popup-message";

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (firstRender)
        {
            await _jsRuntime.InvokeAsync<bool>("focusElement", _id);
        }
    }

    public enum MessageType
    {
        Success,
        Info,
        Failure,
        Error,
        CriticalError,
        Warning,
        Question
    }

    private string _buttonClass
    {
        get
        {
            return Type switch
            {
                MessageType.Success => "btn-success",
                MessageType.Info => "btn-info",
                MessageType.Failure => "btn-secondary",
                MessageType.Error => "btn-danger",
                MessageType.CriticalError => "btn-danger",
                MessageType.Warning => "btn-warning",
                MessageType.Question => throw new Exception("Button should not be appearing for type question"),
                _ => throw new Exception("Un mapped button type")
            };
        }
    }

    private string _colourHex
    {
        get
        {
            return Type switch
            {
                MessageType.Success => "#28A745",
                MessageType.Info => "#17A2B8",
                MessageType.Failure => "#6C757D",
                MessageType.Error => "#DC3545",
                MessageType.CriticalError => "#DC3545",
                MessageType.Warning => "#E1C107",
                MessageType.Question => "#17A2B8",
                _ => throw new Exception("Un mapped button type")
            };
        }
    }

    private string _iconClass
    {
        get
        {
            return Type switch
            {
                MessageType.Success => "fas fa-check-circle",
                MessageType.Info => "fas fa-info-circle",
                MessageType.Failure => "fas fa-bomb",
                MessageType.Error => "fas fa-exclamation-circle",
                MessageType.CriticalError => "fas fa-poop",
                MessageType.Warning => "fas fa-exclamation-triangle",
                MessageType.Question => "fas -fa-question",
                _ => throw new Exception("Un mapped button type")
            };
        }
    }
}
