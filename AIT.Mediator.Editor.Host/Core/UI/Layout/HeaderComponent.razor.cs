﻿using Microsoft.AspNetCore.Components;

namespace AIT.Mediator.Editor.Core.UI.Layout;

partial class HeaderComponent
{
    [CascadingParameter]
    public Func<Task> ToggleLeftColumn { get; set; }


}
