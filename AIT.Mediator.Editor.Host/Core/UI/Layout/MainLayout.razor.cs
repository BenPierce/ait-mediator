﻿namespace AIT.Mediator.Editor.Core.UI.Layout;

partial class MainLayout
{
    private bool _isLeftColumnVisible { get; set; } = true;

    public async Task ToggleLeftColumn()
    {
        _isLeftColumnVisible = !_isLeftColumnVisible;
        await InvokeAsync(StateHasChanged);
    }

}
