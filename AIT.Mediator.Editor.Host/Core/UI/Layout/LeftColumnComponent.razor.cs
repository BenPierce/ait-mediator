﻿using Microsoft.AspNetCore.Components;

namespace AIT.Mediator.Editor.Core.UI.Layout;

partial class LeftColumnComponent
{
    [Inject]
    private NavigationManager NavigationManager { get; set; }

    private void NavigateTo(string url) => NavigationManager.NavigateTo(url);

}
