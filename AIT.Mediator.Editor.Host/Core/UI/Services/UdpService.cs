﻿using AIT.Mediator.Common.Communication;
using AIT.Mediator.Editor.Core.UI.Configuration;
using Microsoft.Extensions.Logging;
using System.Net.Sockets;
using System.Text;

namespace AIT.Mediator.Editor.Core.UI.Services;

class UdpService : IUdpService
{
    private readonly ILogger<UdpService> _logger;
    private readonly ApiConfiguration _apiConfiguration;
    public UdpService(
        ILogger<UdpService> logger,
        ApiConfiguration apiConfiguration
        )
    {
        _logger = logger;
        _apiConfiguration = apiConfiguration;
    }


    public async Task NotifyChange()
    {
        if (string.IsNullOrWhiteSpace(_apiConfiguration?.UdpAddress)) throw new ArgumentNullException(nameof(_apiConfiguration.UdpAddress));

        _logger.LogDebug($"Sending udp message to {_apiConfiguration.UdpAddress}:{_apiConfiguration.UdpPortNo}");

        var message = FormatChangeMessage();
        var data = Encoding.ASCII.GetBytes(message);

        await new UdpClient(_apiConfiguration.UdpAddress, _apiConfiguration.UdpPortNo).SendAsync(data, data.Length);

        _logger.LogDebug($"Udp message sent successfully");
    }

    private string FormatChangeMessage() => $"{_apiConfiguration.EventIdentifier}~CHANGE~A~";
}
