﻿using AIT.Mediator.Common.Core.Extensions;
using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.Common.Mapping.Abstractions.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.Extensions.Logging;
using System.Timers;

namespace AIT.Mediator.Editor.Core.UI;

partial class MediatorCrudComponent<TInterface, TViewModel>
    where TInterface : IItem
    where TViewModel : class, TInterface, new()
{
    [Parameter] public RenderFragment<TInterface> ItemDisplay { get; set; }
    [Parameter] public RenderFragment<TViewModel> EditDisplay { get; set; }
    [Parameter] public string ItemDescription { get; set; } = "item";
    [Parameter] public Func<TViewModel, Task<TInterface>> Save { get; set; }
    [Parameter] public Func<TInterface, Task<TViewModel>> Copy { get; set; }
    [Parameter] public Func<TViewModel, Task> Delete { get; set; }
    [Parameter] public Func<Task<TViewModel>> New { get; set; }
    [Parameter] public Func<Task<List<TInterface>>> GetAll { get; set; }
    [Parameter] public bool IsLeftColumnShowing { get; set; }
    [Parameter] public EventCallback<bool> IsLeftColumnShowingChanged { get; set; }
    [Parameter] public Func<IEnumerable<string>, IEnumerable<TInterface>, Task<IEnumerable<TInterface>>> SearchFilter { get; set; }

    [Inject] private IMapperService _mapperService { get; set; }

    private List<TInterface> _items { get; set; }
    private List<TInterface> _filteredItems { get; set; }

    protected TViewModel _selectedItem { get; set; }

    private System.Timers.Timer _searchTimer;



    protected override async Task OnInitializedAsync()
    {
        try
        {
            _searchTimer = new(300);
            _searchTimer.AutoReset = false;
            _searchTimer.Elapsed += _searchTimer_Elapsed;

            if (Save == null) throw new ArgumentNullException(nameof(Save));
            if (Copy == null) throw new ArgumentNullException(nameof(Copy));
            if (Delete == null) throw new ArgumentNullException(nameof(Delete));
            if (New == null) throw new ArgumentNullException(nameof(New));
            if (GetAll == null) throw new ArgumentNullException(nameof(GetAll));


            await LoadItems();
        }
        catch (Exception ex)
        {
            Error(ex, $"Failed to initialize editor: {ex.Message}");
        }
        finally
        {
            IsReady = true;
        }
    }

    protected override void OnAfterRender(bool firstRender)
    {
        if (firstRender)
        {
            if (IsLeftColumnShowing == false)
            {
                Task.Run(async () =>
                {
                    IsLeftColumnShowing = true;
                    await IsLeftColumnShowingChanged.InvokeAsync(IsLeftColumnShowing);
                });
            }
        }
    }



    #region CRUD
    private async Task SelectItem(TInterface selectedItem)
    {
        _selectedItem = _mapperService.Map<TViewModel>(selectedItem);
    }

    private async Task LoadItems()
    {
        _logger.LogDebug($"Loading all {ItemDescription}");

        _items = await GetAll() ?? new List<TInterface>();
        await Search();

        _logger.LogDebug($"{ItemDescription}s loaded successfully. {_items?.Count ?? 0} {ItemDescription}s loaded");
    }

    private async Task NewItem()
    {
        _selectedItem = await New();
    }

    private async Task SaveItem()
    {
        try
        {
            var updatedItem = await Save(_selectedItem);

            _selectedItem = _mapperService.Map<TViewModel>(updatedItem);

            _items.RemoveAll(i => i.Id == _selectedItem.Id);
            _items.Add(_selectedItem);

            await Search();
        }
        catch (Exception ex)
        {

            Error(ex, $"Failed to save {ItemDescription}: {ex.Message}", false);
        }
    }

    private async Task DeleteItem()
    {
        try
        {
            if (_selectedItem == null)
            {
                _messageService.Warning($"Please select an {ItemDescription}");
            }
            else
            {
                var isConfirmed = await _messageService.Question($"Are you sure you want to delete the selected {ItemDescription}");

                if (isConfirmed)
                {
                    await Delete(_selectedItem);
                    _items.RemoveAll(i => i.Id == _selectedItem.Id);
                    await Search();

                    _selectedItem = default;
                }
            }

        }
        catch (Exception ex)
        {
            Error(ex, $"Failed to delete {ItemDescription}", false);
        }
    }

    private async Task InvalidSaveItem(EditContext editContext)
    {
        _toastService.Warning(editContext.GetValidationMessages().ToDelim(Environment.NewLine));
    }

    private async Task CopyItem(TInterface item)
    {
        try
        {
            _selectedItem = await Copy(item);
        }
        catch (Exception ex)
        {
            Error(ex, $"Failed to copy {ItemDescription}: {ex.Message}", false);
        }
    }
    #endregion

    #region Search

    private bool _isSearching { get; set; }

    private string _searchText;
    protected string SearchText
    {
        get => _searchText;
        set
        {
            if (value?.Trim().Equals(_searchText?.Trim(), StringComparison.OrdinalIgnoreCase) == false)
            {
                _searchText = value;

                _searchTimer.Stop();
                _searchTimer.Start();

                if (_isSearching != true)
                {
                    _isSearching = true;
                    StateHasChangedThreadSafe();
                }
            }
            else
            {
                _searchText = value;
            }
        }
    }

    private void _searchTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
        Task.Run(async () => await Search());
    }

    public async Task Search()
    {
        try
        {
            if (_isSearching)
            {
                _isSearching = false;
                await StateHasChangedThreadSafe();
            }

            var searchParts = _searchText?.Split(" ", StringSplitOptions.RemoveEmptyEntries)?.ToList() ?? new List<string>();

            _filteredItems = (await SearchFilter(searchParts, _items))?.ToList() ?? new List<TInterface>();

            await StateHasChangedThreadSafe();
        }
        catch (Exception ex)
        {
            throw new Exception($"Failed to process search: {ex.Message}", ex);
        }
    }

    #endregion


}


