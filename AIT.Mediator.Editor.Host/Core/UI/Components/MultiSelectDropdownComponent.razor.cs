﻿using AIT.Mediator.Editor.Core.UI.Notifications;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;

namespace AIT.Mediator.Editor.UI.Base.Components;

partial class MultiSelectDropdownComponent<T>
{
    [Parameter]
    public string Id { get; set; }

    [Parameter]
    public string Class { get; set; }

    [Parameter]
    public string Style { get; set; }

    [Parameter]
    public IEnumerable<T> Items { get; set; }

    [Parameter]
    public List<T> SelectedItems { get; set; }

    [Parameter]
    public EventCallback<List<T>> SelectedItemsChanged { get; set; }

    [Parameter]
    public RenderFragment HeaderTemplate { get; set; }

    [Parameter]
    public RenderFragment<T> RowTemplate { get; set; }

    [Parameter]
    public RenderFragment<T> SelectedTemplate { get; set; }

    [Parameter]
    public Func<IEnumerable<T>, IEnumerable<string>, IEnumerable<T>> SearchFilter { get; set; }

    [Parameter]
    public string Placeholder { get; set; }


    [Inject]
    private IMessageService _messageService { get; set; }

    [Inject]
    private IJSRuntime _jsRuntime { get; set; }

    private string _id { get; set; }
    private string _textInputId { get; set; }

    private string _text;
    private string _searchText
    {
        get => _text;
        set
        {
            if (_text != value)
            {
                _text = value;

                //Cause refresh which applies filter
                Task.Run(async () =>
                {
                    await InvokeAsync(StateHasChanged);
                });
            }
        }

    }

    private string _placeHolder => SelectedItems?.Any() != true ? Placeholder : null;

    private bool _isDroppedDown { get; set; }
    private bool _isInitialized { get; set; }



    private IEnumerable<T> _filteredItems => Filter();

    private IEnumerable<T> Filter()
    {
        if (string.IsNullOrWhiteSpace(_searchText))
        {
            return Items;
        }
        else
        {
            var searchParts = _searchText.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            return SearchFilter(Items, searchParts);
        }
    }

    protected override void OnInitialized()
    {
        try
        {
            if (SelectedItems == null) throw new Exception($"{nameof(SelectedItems)} is null. Please provide an initialised collection (ie new List<>)");
            if (RowTemplate == null) throw new ArgumentNullException(nameof(RowTemplate));
            if (SelectedTemplate == null) throw new ArgumentNullException(nameof(SelectedTemplate));
            if (SelectedTemplate == null) throw new ArgumentNullException(nameof(SearchFilter));

            _id = Id ?? Guid.NewGuid().ToString();
            _textInputId = Guid.NewGuid().ToString();
        }
        catch (Exception ex)
        {
            _messageService.Error($"{nameof(MultiSelectDropdownComponent<T>)} error", ex);
        }
        finally
        {
            _isInitialized = true;
        }
    }

    private void OnItemClick(T item)
    {
        if (IsItemSelected(item))
        {
            SelectedItems.Remove(item);
        }
        else
        {
            _searchText = null;
            SelectedItems.Add(item);
        }
    }

    private bool IsItemSelected(T item) => SelectedItems.Contains(item);


    private async Task OnInputFocus(FocusEventArgs focusEventArgs)
    {
        if (await _jsRuntime.InvokeAsync<bool>("isElementChildInFocus", _textInputId) == false)
        {
            await _jsRuntime.InvokeVoidAsync("focusElement", _textInputId);
        }

        _isDroppedDown = await _jsRuntime.InvokeAsync<bool>("isElementChildInFocus", _id);
    }

    private async Task OnLostFocus(FocusEventArgs focusEventArgs)
    {
        _isDroppedDown = await _jsRuntime.InvokeAsync<bool>("isElementChildInFocus", _id);
    }

    private async Task OnKeyUp(KeyboardEventArgs keyboardEventArgs)
    {
        if (keyboardEventArgs?.Key.Equals("Backspace", StringComparison.OrdinalIgnoreCase) == true
            && string.IsNullOrWhiteSpace(_searchText))
        {
            var lastItem = SelectedItems.LastOrDefault();

            if (lastItem != null)
            {
                SelectedItems.Remove(lastItem);
            }
        }
        if (keyboardEventArgs?.Key.Equals("Enter", StringComparison.OrdinalIgnoreCase) == true
            && _filteredItems?.Count() == 1)
        {
            OnItemClick(_filteredItems.First());
        }
    }

}
