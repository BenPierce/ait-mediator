﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;

namespace AIT.Mediator.Editor.UI.Base.Components;

partial class ContextMenuComponent
{
    [Parameter] public RenderFragment Body { get; set; }
    [Parameter] public RenderFragment DropDown { get; set; }

    [Inject] public IJSRuntime _jsRuntime { get; set; }

    protected Guid ContextMenuId { get; set; }

    private bool _isOpen { get; set; }

    protected int Left { get; set; }
    protected int Top { get; set; }

    protected override void OnInitialized()
    {
        ContextMenuId = Guid.NewGuid();

    }

    private async Task OnRightClick(MouseEventArgs mouseEventArgs)
    {
        try
        {
            if (_isOpen == false)
            {

                Left = (int)mouseEventArgs.ClientX - 5 < 0
                     ? (int)mouseEventArgs.ClientX
                     : (int)mouseEventArgs.ClientX - 5;

                Top = (int)mouseEventArgs.ClientY - 5 < 0
                    ? (int)mouseEventArgs.ClientY
                    : (int)mouseEventArgs.ClientY - 5;

                _isOpen = true;
            }
            else
            {
                _isOpen = false;
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, $"Failed to process right click and display context menu: {ex.Message}");
            await _messageService.ErrorAsync("Failed to display right click menu", ex);
        }
    }

    protected async Task LostFocus() => _isOpen = false;
}


public class ContextMenuSize
{
    public int Left { get; set; }
    public int Top { get; set; }
}
