﻿namespace AIT.Mediator.Editor.Core.UI.Configuration;

class ApiConfiguration
{
    public string EventIdentifier { get; set; }
    public string UdpAddress { get; set; }
    public int UdpPortNo { get; set; }
}
