﻿using Blazored.Modal;
using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;

namespace AIT.Mediator.Editor.Core.UI;

static class ModalExtensions
{
    public static IModalReference Show<T>(this IModalService modalService, ModalParameters modalParameters) where T : ComponentBase
    {
        return modalService.Show<T>("", modalParameters);
    }

    public static IModalReference Show<T>(this IModalService modalService, ModalParameters modalParameters, ModalOptions modalOptions) where T : ComponentBase
    {
        return modalService.Show<T>("", modalParameters, modalOptions);
    }

    public static IModalReference Show<T>(this IModalService modalService, ModalOptions modalOptions) where T : ComponentBase
    {
        return modalService.Show<T>("", modalOptions);
    }
}
