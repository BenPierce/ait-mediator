﻿using AIT.PubSub;

namespace AIT.Mediator.Editor.Core.UI.Toast;

public interface IToastService
{
    void Success(string message, string title = "Success");
    void Info(string message, string title = "Info");
    void Warning(string message, string title = "Warning");
    void Danger(string message, string title = "Error");
    void Primary(string message, string title = "");
}

class ToastService : IToastService
{
    private readonly IPubSubScopedService _pubSubScopedService;

    public ToastService(
        IPubSubScopedService pubSubScopedService
        )
    {
        _pubSubScopedService = pubSubScopedService;
    }


    public void Danger(string message, string title = "Error")
    {
        _pubSubScopedService.Publish<ToastMessage>(new(
            type: ToastType.Danger,
            message: message,
            header: title
            ));
    }

    public void Info(string message, string title = "Info")
    {
        _pubSubScopedService.Publish<ToastMessage>(new(
            type: ToastType.Info,
            message: message,
            header: title
            ));
    }

    public void Primary(string message, string title = "")
    {
        _pubSubScopedService.Publish<ToastMessage>(new(
            type: ToastType.Primary,
            message: message,
            header: title
            ));
    }

    public void Success(string message, string title = "Success")
    {
        _pubSubScopedService.Publish<ToastMessage>(new(
            type: ToastType.Success,
            message: message,
            header: title
            ));
    }

    public void Warning(string message, string title = "Warning")
    {
        _pubSubScopedService.Publish<ToastMessage>(new(
            type: ToastType.Warning,
            message: message,
            header: title
            ));
    }
}
