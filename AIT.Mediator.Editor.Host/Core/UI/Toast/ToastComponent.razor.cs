﻿using AIT.PubSub;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;

namespace AIT.Mediator.Editor.Core.UI.Toast;

partial class ToastComponent : IDisposable, ISubscriber<ToastMessage>
{
    [Parameter]
    public RenderFragment ChildContent { get; set; }

    [Inject]
    private ILogger<ToastComponent> _logger { get; set; }

    [Inject]
    private IPubSubScopedService _pubSubScopedService { get; set; }


    private SemaphoreSlim _lock = new SemaphoreSlim(1, 1);

    private List<ToastDisplay> _toastStack { get; set; } = new();
    public async Task Remove(ToastDisplay toastDisplay)
    {
        await _lock.WaitAsync();

        try
        {
            _toastStack.Remove(toastDisplay);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Failed to remove toast from display collection");
        }
        finally
        {
            _lock.Release();
        }

    }
    public async Task Add(ToastDisplay toastDisplay)
    {
        await _lock.WaitAsync();

        try
        {
            _toastStack.Add(toastDisplay);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Failed to add toast to display collection");
        }
        finally
        {
            _lock.Release();
        }

    }

    protected override void OnInitialized()
    {
        _pubSubScopedService.Subscribe(this);
    }

    public void Dispose()
    {
        _pubSubScopedService.Unsubscribe(this);
    }

    public async Task OnEvent(ToastMessage message)
    {
        try
        {
            var display = new ToastDisplay(
                toastMessage: message,
                changedAction: () => InvokeAsync(StateHasChanged),
                removeAction: async (message) =>
                {
                    await Remove(message);
                    await InvokeAsync(StateHasChanged);
                });

            await Add(display);

            await InvokeAsync(StateHasChanged);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Failed to display toast message");
        }
    }

    public class ToastDisplay
    {
        public Guid Key { get; set; }
        public ToastMessage ToastMessage { get; init; }
        private Func<Task> _changedAction { get; init; }
        private Func<ToastDisplay, Task> _removeAction { get; init; }
        private Task _displayProcess { get; set; }

        public string Class { get; private set; }
        public string IconClass { get; set; }

        public ToastDisplay(
            ToastMessage toastMessage,
            Func<Task> changedAction,
            Func<ToastDisplay, Task> removeAction
            )
        {
            Key = Guid.NewGuid();
            Class = "start animated";
            IconClass = $"{toastMessage.Type.ToString().ToLower()}-background-colour";

            ToastMessage = toastMessage;
            _changedAction = changedAction;
            _removeAction = removeAction;

            _displayProcess = Task.Run(async () =>
            {
                Thread.Sleep(50);

                Class = "animated";
                await _changedAction();

                Thread.Sleep(5000);

                Class = "hide animated";
                await _changedAction();

                Thread.Sleep(1000);

                Class = "hide";

                await _removeAction(this);
            });
        }
    }
}
