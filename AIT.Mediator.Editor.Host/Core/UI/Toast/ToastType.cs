﻿namespace AIT.Mediator.Editor.Core.UI.Toast;

public enum ToastType
{
    Success,
    Info,
    Warning,
    Danger,
    Primary
}
