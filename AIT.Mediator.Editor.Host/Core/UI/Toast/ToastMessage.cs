﻿namespace AIT.Mediator.Editor.Core.UI.Toast;

public class ToastMessage
{
    public ToastType Type { get; init; }
    public string Header { get; init; }
    public string Message { get; init; }

    public ToastMessage(
        ToastType type,
        string message,
        string header = null
        )
    {
        Type = type;
        Message = message;
        Header = header;
    }
}
