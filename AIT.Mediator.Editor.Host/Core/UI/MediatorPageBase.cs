﻿using Microsoft.AspNetCore.Components;

namespace AIT.Mediator.Editor.Core.UI;

public abstract class MediatorPageBase<T> : MediatorComponentBase<T>
{
    [Inject]
    protected NavigationManager NavigationManager { get; set; }

}
