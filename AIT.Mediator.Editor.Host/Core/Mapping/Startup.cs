﻿using AIT.Mediator.Common.Mapping.Abstractions.Services;
using System.Reflection;

namespace AIT.Mediator.Editor.Core.Mapping;

public static class Startup
{
    public static void AddMapping(this IServiceCollection services)
    {
        services.AddTransient<IMapperService, MapperService>();

        IEnumerable<Assembly> assemblies = AppDomain.CurrentDomain.GetAssemblies().
            Where(assembly => assembly.FullName.Contains("AIT.Mediator", StringComparison.OrdinalIgnoreCase));

        services.AddAutoMapper(assemblies);
    }
}
