﻿using AIT.Mediator.Common.Outbound.Abstractions;
using AIT.Mediator.Editor.Outbound.UI;
using AutoMapper;

namespace AIT.Mediator.Editor.Core.Mapping;

class OutboundMappingProfile : Profile
{
    public OutboundMappingProfile()
    {
        CreateMap<IOutboundRoute, OutboundRouteVM>();
    }
}

