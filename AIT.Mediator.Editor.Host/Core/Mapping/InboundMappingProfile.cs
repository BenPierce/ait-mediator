﻿using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.Editor.Apps.UI;
using AIT.Mediator.Editor.Inbound.UI;
using AutoMapper;

namespace AIT.Mediator.Editor.Core.Mapping;

class InboundMappingProfile : Profile
{
    public InboundMappingProfile()
    {
        CreateMap<IApp, AppVM>();


        CreateMap<IInboundRouteAppAccess, InboundRouteAppAccessVM>().ConvertUsing<InboundRouteAppAccessVMConverter>();

        CreateMap<IInboundRoute, InboundRouteVM>()
            .ForMember(
            dm => dm.AppAccessVMs,
            opts => opts.MapFrom(src => src.AppAccess)
            ).IgnoreAllPropertiesWithAnInaccessibleSetter();

    }
}

