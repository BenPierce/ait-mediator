﻿using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.Editor.Inbound.UI;
using AutoMapper;

namespace AIT.Mediator.Editor.Core.Mapping;

class InboundRouteAppAccessVMConverter : ITypeConverter<IInboundRouteAppAccess, InboundRouteAppAccessVM>
{
    public InboundRouteAppAccessVM Convert(IInboundRouteAppAccess source, InboundRouteAppAccessVM destination, ResolutionContext context)
    {
        return new InboundRouteAppAccessVM(source);
    }
}
