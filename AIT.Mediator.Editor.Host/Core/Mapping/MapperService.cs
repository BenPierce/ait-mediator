﻿using AIT.Mediator.Common.Mapping.Abstractions.Services;
using AutoMapper;

namespace AIT.Mediator.Editor.Core.Mapping;

class MapperService : IMapperService
{
    private readonly IMapper _mapper;

    public MapperService(
        IMapper mapper
        )
    {
        _mapper = mapper;
    }

    public TTo Map<TTo>(object obj)
    {
        return _mapper.Map<TTo>(obj);
    }

    public TTo Map<TFrom, TTo>(TFrom obj)
    {
        return _mapper.Map<TFrom, TTo>(obj);
    }

    public object Map(object obj, Type typeFrom, Type typeTo)
    {
        return _mapper.Map(obj, typeFrom, typeTo);
    }
}
