﻿using AIT.Mediator.Common.Inbound.Abstractions;
using Microsoft.AspNetCore.Components;
using AIT.Mediator.Common.Mapping.Abstractions.Services;
using AIT.Mediator.Common.Communication;
using AIT.Mediator.Database.Inbound;

namespace AIT.Mediator.Editor.Inbound.UI;

partial class InboundRoutePage
{
    [Inject] private IInboundDataSource _dataSource { get; set; }
    [Inject] private IUdpService _udpService { get; set; }
    [Inject] private IMapperService _mapperService { get; set; }

    private IEnumerable<IApp> _apps { get; set; }

    protected override async Task OnInitializedAsync()
    {
        _apps = await _dataSource.GetApps();
    }

    private async Task<List<IInboundRoute>> GetAllRoutes()
    {
        return (await _dataSource.GetInboundRoutes())?.ToList() ?? new List<IInboundRoute>();
    }

    private async Task<IInboundRoute> Save(InboundRouteVM inboundRouteVM)
    {
        var route = await _dataSource.UpsertInboundRoute(new(
            route: inboundRouteVM
            ),
            validAppIds: inboundRouteVM.AppAccess.Where(aa => aa.IsAccessGranted && aa.IsAppDisabled == false)?.Select(aa => aa.AppId)?.ToList()
            );

        await _udpService.NotifyChange();

        _toastService.Success($"{inboundRouteVM?.Name} saved successfully");
        return route;
    }

    private async Task<InboundRouteVM> Copy(IInboundRoute inboundRoute)
    {
        var inboundRouteVM = _mapperService.Map<InboundRouteVM>(inboundRoute);

        inboundRouteVM.RouteId = null;
        inboundRouteVM.Name = $"{inboundRouteVM.Name} - Copy";

        _toastService.Success($"{inboundRouteVM?.Name} copied successfully");
        return inboundRouteVM;
    }

    private async Task Delete(InboundRouteVM inboundRouteVM)
    {
        await _dataSource.DeleteInboundRoute(new(
            routeId: inboundRouteVM.Id
            ));

        _toastService.Success($"{inboundRouteVM?.Name} deleted successfully");
        await _udpService.NotifyChange();
    }

    private async Task<InboundRouteVM> New()
    {
        var inboundRouteVM = new InboundRouteVM();
        inboundRouteVM.AppAccessVMs = _apps?.Select(a => new InboundRouteAppAccessVM(a))?.ToList();

        return inboundRouteVM;
    }

    private async Task<IEnumerable<IInboundRoute>> SearchRoutes(IEnumerable<string> searchParts, IEnumerable<IInboundRoute> items)
    {
        try
        {
            IEnumerable<IInboundRoute> routes = items?.ToList();

            if (routes?.Any() == true && searchParts?.Any() == true)
            {
                foreach (var searchPart in searchParts)
                {
                    var working = routes.Where(route => route.Name.Contains(searchPart, StringComparison.OrdinalIgnoreCase)
                                                     || route.UrlMask.Contains(searchPart, StringComparison.OrdinalIgnoreCase)
                                                     || route.AppAccess?.Any(aa => aa.AppName.Contains(searchPart, StringComparison.OrdinalIgnoreCase) && aa.IsAccessGranted) == true).ToList();

                    routes = routes.Intersect(working);
                }
            }

            return routes;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Route search error");
            await _messageService.ErrorAsync("Failed to process search", ex);

            return null;
        }
    }

}
