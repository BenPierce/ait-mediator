﻿using AIT.Mediator.Common.Inbound.Abstractions;

namespace AIT.Mediator.Editor.Inbound.UI;

public class InboundRouteAppAccessVM : IInboundRouteAppAccess
{
    public int? AccessId { get; private set; }
    public int AppId { get; private set; }
    public string AppName { get; private set; }
    public bool IsAccessGranted { get; set; }
    public bool IsAppDisabled { get; private set; }

    public InboundRouteAppAccessVM(
        IInboundRouteAppAccess inboundRouteAppAccess
        )
    {
        if (inboundRouteAppAccess is null) throw new ArgumentNullException(nameof(inboundRouteAppAccess));

        AccessId = inboundRouteAppAccess.AccessId;
        AppId = inboundRouteAppAccess.AppId;
        AppName = inboundRouteAppAccess.AppName;
        IsAccessGranted = inboundRouteAppAccess.IsAccessGranted;
        IsAppDisabled = inboundRouteAppAccess.IsAppDisabled;
    }

    public InboundRouteAppAccessVM(
        IApp app
        )
    {
        if (app is null) throw new ArgumentNullException(nameof(app));

        AppId = (int)app.AppId;
        AppName = app.Name;
        IsAppDisabled = app.IsDisabled;
    }
}
