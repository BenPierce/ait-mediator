﻿using AIT.Mediator.Common.Inbound.Abstractions;
using FluentValidation;

namespace AIT.Mediator.Editor.Inbound.UI;

public class InboundRouteVM : IInboundRoute
{
    public int? RouteId { get; set; }
    public string Name { get; set; }
    public string UrlMask { get; set; }
    public string Sql { get; set; }
    public bool DeserializeRequest { get; set; }
    public bool SerializeResponse { get; set; }
    public bool IsDocumentRequest { get; set; }

    public int? Id => RouteId;

    public IEnumerable<InboundRouteAppAccessVM> AppAccessVMs { get; set; }

    public IEnumerable<IInboundRouteAppAccess> AppAccess => AppAccessVMs;

    public class Validator : AbstractValidator<InboundRouteVM>
    {
        public Validator()
        {
            RuleFor(a => a.Name)
                .NotEmpty()
                .WithMessage("Name is required");

            RuleFor(a => a.UrlMask)
                .NotEmpty()
                .WithMessage("Route is required");

            RuleFor(a => a.Sql)
                .NotEmpty()
                .WithMessage("SQL is required");
        }
    }
}
