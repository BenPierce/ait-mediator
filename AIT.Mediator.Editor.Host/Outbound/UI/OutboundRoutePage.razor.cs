﻿using AIT.Mediator.Common.Mapping.Abstractions.Services;
using AIT.Mediator.Common.Outbound.Abstractions;
using AIT.Mediator.Common.Outbound.Data;
using Microsoft.AspNetCore.Components;

namespace AIT.Mediator.Editor.Outbound.UI;

partial class OutboundRoutePage
{
    [Inject] private IOutboundDataSource _outboundDataSource { get; set; }
    [Inject] private IMapperService _mapperService { get; set; }

    private async Task<List<IOutboundRoute>> GetAllOutboundRoutes()
    {
        return (await _outboundDataSource.GetOutboundRoutes())?.ToList() ?? new List<IOutboundRoute>();
    }

    private async Task<IOutboundRoute> Save(OutboundRouteVM OutboundRouteVM)
    {
        var OutboundRoute = await _outboundDataSource.UpsertOutboundRoute(new UpsertOutboundRouteQueryParameter(
            outboundRoute: OutboundRouteVM
            ));

        //await _udpService.NotifyChange();
        _toastService.Success($"{OutboundRouteVM?.Name} saved successfully");

        return OutboundRoute;
    }

    private async Task Delete(OutboundRouteVM OutboundRouteVM)
    {
        await _outboundDataSource.DeleteOutboundRoute(new DeleteOutboundRouteQueryParameter(
            routeId: OutboundRouteVM.Id
            ));

        _toastService.Success($"{OutboundRouteVM?.Name} deleted successfully");
        //await _udpService.NotifyChange();
    }

    private async Task<OutboundRouteVM> Copy(IOutboundRoute outboundRoute)
    {
        var outboundRouteVM = _mapperService.Map<OutboundRouteVM>(outboundRoute);

        outboundRouteVM.RouteId = null;
        outboundRouteVM.Name = $"{outboundRouteVM.Name} - Copy";

        _toastService.Success($"{outboundRoute?.Name} copied successfully");
        return outboundRouteVM;
    }

    private async Task<OutboundRouteVM> New() => new OutboundRouteVM();

    private async Task<IEnumerable<IOutboundRoute>> SearchOutboundRoutes(IEnumerable<string> searchParts, IEnumerable<IOutboundRoute> items)
    {
        try
        {
            IEnumerable<IOutboundRoute> OutboundRoutes = items?.ToList();

            if (OutboundRoutes?.Any() == true && searchParts?.Any() == true)
            {
                foreach (var searchPart in searchParts)
                {
                    var working = OutboundRoutes.Where(OutboundRoute => OutboundRoute.Name.Contains(searchPart, StringComparison.OrdinalIgnoreCase)).ToList();

                    OutboundRoutes = OutboundRoutes.Intersect(working);
                }
            }

            return OutboundRoutes;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Outbound Route search error");
            await _messageService.ErrorAsync("Failed to process search", ex);

            return null;
        }
    }
}
