﻿using AIT.Mediator.Common.Core.Enums;
using AIT.Mediator.Common.Outbound.Abstractions;
using FluentValidation;

namespace AIT.Mediator.Editor.Outbound.UI;

public class OutboundRouteVM : IOutboundRoute
{
    public int? RouteId { get; set; }
    public string Name { get; set; }
    public RestType RequestType { get; set; }
    public string RequestUrlSql { get; set; }
    public string RequestHeadersSql { get; set; }
    public bool DeserializeRequestHeaders { get; set; }
    public string RequestBodySql { get; set; }
    public bool SerializeRequestBody { get; set; }
    public string ResponseSql { get; set; }
    public string ResponseErrorSql { get; set; }
    public bool DeserializeResponse { get; set; }
    public string ProceedSql { get; set; }
    public int? Id => RouteId;

    public class Validator : AbstractValidator<OutboundRouteVM>
    {
        public Validator()
        {
            RuleFor(a => a.Name)
                .NotEmpty()
                .WithMessage("Name is required");

            RuleFor(a => a.RequestType).NotEqual(RestType.INVALID)
                .WithMessage("Invalid request type");

            RuleFor(a => a.RequestUrlSql)
                .NotEmpty()
                .WithMessage("Request URL SQL is required");
        }
    }
}
