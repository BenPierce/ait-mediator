﻿function focusElement(id) {
	var element = document.getElementById(id);
	element.focus();
	return true;
}

function isElementChildInFocus(parentId) {
	var element = document.getElementById(parentId);	

	var elementHasFocus = element.contains(document.activeElement);
	var documentHasFocus = document.hasFocus(); //Ensures the browser has focus
	//console.log("element " + parentId + " focus: " + elementHasFocus);
	//console.log("document has focus: " + documentHasFocus);

	return elementHasFocus && documentHasFocus;
}