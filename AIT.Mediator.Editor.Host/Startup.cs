using AIT.Mediator.Database;
using AIT.Mediator.Common.Core.Logging;
using AIT.IOC;
using AIT.Mediator.Common.Communication;
using AIT.PubSub;
using Blazored.Modal;
using FluentValidation;
using AIT.Mediator.Editor.Outbound.UI;
using AIT.Mediator.Editor.Core.Mapping;
using AIT.Mediator.Editor.Apps.UI;
using AIT.Mediator.Editor.Core.UI.Toast;
using AIT.Mediator.Editor.Inbound.UI;
using AIT.Mediator.Editor.Core.UI.Services;
using AIT.Mediator.Editor.Core.UI.Configuration;
using AIT.Mediator.Editor.Core.UI.Notifications;

namespace AIT.Mediator.Editor;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddRazorPages();

        services.AddServerSideBlazor();

        services.AddLogging("AIT.Mediator.Host");
        services.AddDatabase();
        services.AddMapping();

        services.AddConfigurationTransient<ApiConfiguration>("Api");
        services.AddBlazoredModal();


        services.AddTransient<IValidator<InboundRouteVM>, InboundRouteVM.Validator>();
        services.AddTransient<IValidator<AppVM>, AppVM.AppVMValidator>();

        services.AddTransient<IValidator<OutboundRouteVM>, OutboundRouteVM.Validator>();

        services.AddTransient<IUdpService, UdpService>();

        services.AddPubSubScoped();
        services.AddTransient<IToastService, ToastService>();
        services.AddTransient<IMessageService, MessageService>();

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();

        app.UseStaticFiles();

        app.UseRouting();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapBlazorHub();
            endpoints.MapFallbackToPage("/_Host");
        });
    }
}
