using AIT.Mediator.Database;
using AIT.Mediator.Common.Outbound.Abstractions;
using AIT.Mediator.API.Outbound.Services;
using AIT.IOC.Factories;
using AIT.Mediator.API.Inbound;
using AIT.PubSub;
using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.API.Inbound.Services;
using AIT.Database.Listener;
using AIT.Mediator.API.Inbound.Services.PDF;
using AIT.Mediator.Common.Core.Logging;

namespace AIT.Mediator.API;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddLogging("Mediator.API");
        services.AddDatabase();
        services.AddPubSubSingleton();
        services.AddPDF();

        services.AddHostedService<InboundRouteCacheService>();

        services.AddFactory<IInboundRequestService, InboundRequestService>(Scope.Singleton);

        services.AddFactory<IOutboundRequestService, OutboundRequestService>(Scope.Singleton);

        services.AddDatabaseListener<DatabaseEventHandlerService>("DatabaseListener");

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.InboundRoute();
    }
}
