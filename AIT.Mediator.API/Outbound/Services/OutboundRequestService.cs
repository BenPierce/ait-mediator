﻿using AIT.Mediator.API.Outbound.Models;
using AIT.Mediator.Common.Outbound.Abstractions;
using AIT.Mediator.Common.Outbound.Data;
using System.Data;
using AIT.Mediator.Common.Core.Enums;
using AIT.Mediator.Common.Core.Extensions;

namespace AIT.Mediator.API.Outbound.Services;

class OutboundRequestService : IOutboundRequestService
{
    private readonly ILogger<OutboundRequestService> _logger;
    private readonly IOutboundDataSource _outboundDataSource;

    public OutboundRequestService(
        ILogger<OutboundRequestService> logger,
        IOutboundDataSource outboundDataSource
        )
    {
        _logger = logger;
        _outboundDataSource = outboundDataSource;
    }


    public async Task RequestById(int routeId, IEnumerable<string> parameters)
    {
        try
        {
            var outboundRoute = await _outboundDataSource.GetRoute(new GetOutboundRouteQueryParameter(
                routeId: routeId
                )) ?? throw new Exception("No matching route found");

            var shouldProceed = await ConfirmProceed(outboundRoute, parameters);

            if (shouldProceed == true)
            {
                var outboundRequest = new OutboundRestRequest(outboundRoute);

                outboundRequest.RestType = outboundRoute.RequestType;

                outboundRequest.Url = await BuildUrl(outboundRoute, parameters);
                outboundRequest.Headers = await BuildHeaders(outboundRoute, parameters);
                outboundRequest.Body = await BuildBody(outboundRoute, parameters);

                await ProcessRequest(outboundRequest, parameters);
            }

        }
        catch (Exception ex)
        {
            _logger.LogError(ex, $"Failed to process outbound request for id '{routeId}': {ex.Message}");
        }
    }

    private async Task<bool> ConfirmProceed(
        IOutboundRoute outboundRoute,
        IEnumerable<string> parameters
        )
    {
        if (string.IsNullOrWhiteSpace(outboundRoute.ProceedSql))
        {
            _logger.LogDebug("No proceed SQL set, continuing");
            return true;
        }
        else
        {
            _logger.LogDebug("Proceed SQL set, confirming whether to continue");

            var shouldProceed = await _outboundDataSource.GetProceed(
                proceedSql: outboundRoute.ProceedSql,
                parameter: new(parameters?.ToArray())
                );

            if (shouldProceed != true)
            {
                _logger.LogDebug("Proceed was not confirmed");
            }
            else
            {
                _logger.LogDebug("Proceed was confirmed");
            }

            return shouldProceed;
        }
    }


    private async Task ProcessRequest(OutboundRestRequest outboundRequest, IEnumerable<string> parameters)
    {
        _logger.LogDebug("Sending outbound request");

        var requestMessage = BuildRequestMessage(outboundRequest);

        var client = new HttpClient();

        var response = await client.SendAsync(requestMessage);

        await ProcessOutboundResponse(response, outboundRequest, parameters);
    }

    private async Task ProcessOutboundResponse(HttpResponseMessage response, OutboundRestRequest request, IEnumerable<string> parameters)
    {
        var responseVariables = await GetResponseVariables(response, request);

        if (parameters?.Any() == true)
        {
            var parameterArray = parameters.ToArray();

            for (int i = 0; i < parameterArray.Length; i++)
            {
                responseVariables.Add($"Variable{i + 1}", parameterArray[i]);
            }
        }


        if (response?.IsSuccessStatusCode != true)
        {
            if (string.IsNullOrWhiteSpace(request.OutboundRoute.ResponseErrorSql) != true)
            {
                try
                {
                    await _outboundDataSource.ExecuteRouteResponse(
                                  query: request.OutboundRoute.ResponseErrorSql,
                                  parameters: responseVariables
                                  );
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Failed to process error resonse: {ex.Message}");
                }
            }

            throw new Exception($"Request failed with status code: {response?.StatusCode}");
        }
        else
        {
            _logger.LogDebug("Processing response");

            if (response.Content == null)
            {
                _logger.LogDebug("No response body");
            }
            else if (string.IsNullOrWhiteSpace(request.OutboundRoute.ResponseSql))
            {
                _logger.LogDebug("Body present but no response SQL set");
            }
            else
            {
                _logger.LogDebug("Executing response SQL");



                await _outboundDataSource.ExecuteRouteResponse(
                  query: request.OutboundRoute.ResponseSql,
                  parameters: responseVariables
                  );

                _logger.LogDebug("Response SQL executed successfully");
            }
        }

        _logger.LogDebug("Response processing complete");
    }

    private async Task<IDictionary<string, object>> GetResponseVariables(HttpResponseMessage response, OutboundRestRequest request)
    {
        _logger.LogDebug("Building response variables");

        IEnumerable<KeyValuePair<string, string>> headerVariables = GetResponseHeaderVariables(response);

        IEnumerable<KeyValuePair<string, string>> bodyVariables = await GetResponseBodyVariables(response, request.OutboundRoute);

        var responseVariables = headerVariables.Concat(bodyVariables)
                              .GroupBy(d => d.Key)
                              .ToDictionary(d => d.Key, d => (object)d.First().Value);

        _logger.LogDebug("Response variables built successfully");

        return responseVariables;
    }

    private HttpRequestMessage BuildRequestMessage(OutboundRestRequest request)
    {
        try
        {
            var message = new HttpRequestMessage(
                    method: request.RestType.ToHttpMethod(),
                    requestUri: request.Url
                    );

            if (request.Headers?.Any() == true)
            {
                foreach (var header in request.Headers)
                {
                    message.Headers.Add(header.Key, header.Value);
                }
            }

            if (request.Body != null)
            {
                message.Content = new StringContent(request.Body);
            }

            return message;
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to build request", ex);
        }
    }



    private async Task<string> BuildUrl(
        IOutboundRoute outboundRoute,
        IEnumerable<string> parameters
        )
    {
        _logger.LogDebug("Building outbound URL");

        var url = await _outboundDataSource.GetRouteUrl(
            outboundUrlSql: outboundRoute.RequestUrlSql,
            parameter: new(parameters?.ToArray())
            )
                                ?? throw new Exception("No URL retrieved from database");

        _logger.LogDebug($"URL '{url}' retrieved from database");

        Uri uriResult;
        bool isValidUrl = Uri.TryCreate(url, UriKind.Absolute, out uriResult)
            && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

        return isValidUrl == false
               ? throw new Exception($"Url '{url}' did not pass validation")
               : url;
    }

    private async Task<IDictionary<string, string>> BuildHeaders(
        IOutboundRoute outboundRoute,
        IEnumerable<string> parameters
        )
    {
        _logger.LogDebug("Building outbound headers");

        IDictionary<string, string> outboundHeaders = null;

        if (string.IsNullOrWhiteSpace(outboundRoute.RequestHeadersSql))
        {
            _logger.LogDebug("No headers sql defined");
        }
        else
        {
            _logger.LogDebug($"SerializeRequestHeaders set to {outboundRoute.DeserializeRequestHeaders}");

            if (outboundRoute.DeserializeRequestHeaders == false)
            {
                var unserializedHeaders = await _outboundDataSource.GetHeaderUnserialized(
                    outboundHeadersSql: outboundRoute.RequestHeadersSql,
                    parameter: new(parameters?.ToArray())
                    );

                if (unserializedHeaders == null)
                {
                    _logger.LogDebug("No headers retrieved from database");
                }
                else
                {
                    _logger.LogDebug($"{unserializedHeaders?.Count() ?? 0} headers retrieved");

                    outboundHeaders = unserializedHeaders;
                }
            }
            else
            {
                var serializedHeaders = await _outboundDataSource.GetHeadersSerialized(
                    outboundHeadersSql: outboundRoute.RequestHeadersSql,
                    parameter: new(parameters?.ToArray())
                    );

                if (string.IsNullOrWhiteSpace(serializedHeaders))
                {
                    _logger.LogDebug("No headers retrieved from database");
                }
                else
                {
                    var headerDictionary = JsonSerializer.Deserialize<Dictionary<string, JsonElement>>(serializedHeaders);

                    _logger.LogDebug($"{headerDictionary?.Count() ?? 0} headers retrieved");

                    outboundHeaders = headerDictionary?.Select(bd => new KeyValuePair<string, string>(bd.Key, bd.Value.GetRawText()?.Trim('"'))).ToDictionary(a => a.Key, a => a.Value);
                }
            }
        }


        return outboundHeaders;
    }

    private async Task<string> BuildBody(
        IOutboundRoute outboundRoute,
        IEnumerable<string> parameters
        )
    {
        _logger.LogDebug("Building outbound body");

        string outboundBody = null;

        if (string.IsNullOrWhiteSpace(outboundRoute.RequestBodySql))
        {
            _logger.LogDebug("No body sql defined");
        }
        else
        {
            _logger.LogDebug($"SerializeRequestBody set to {outboundRoute.SerializeRequestBody}");

            if (outboundRoute.SerializeRequestBody)
            {
                var unserializedBody = await _outboundDataSource.GetBodyUnserialized(
                    outboundBodySql: outboundRoute.RequestBodySql,
                    parameter: new(parameters?.ToArray())
                    );

                if (unserializedBody?.Any() != true)
                {
                    _logger.LogDebug("No body retrieved from database");
                }
                else
                {
                    _logger.LogDebug($"Body retrieved");

                    outboundBody = JsonSerializer.Serialize(unserializedBody);
                }
            }
            else
            {
                var serializedBody = await _outboundDataSource.GetBodySerialized(
                    outboundBodySql: outboundRoute.RequestBodySql,
                    parameter: new(parameters?.ToArray())
                    );

                if (string.IsNullOrWhiteSpace(serializedBody))
                {
                    _logger.LogDebug("No body retrieved from database");
                }
                else
                {
                    _logger.LogDebug($"Body retrieved");

                    outboundBody = serializedBody;
                }
            }
        }

        return outboundBody;
    }



    private IEnumerable<KeyValuePair<string, string>> GetResponseHeaderVariables(HttpResponseMessage response)
    {
        var headerVariables = response.Headers.Select(header => new KeyValuePair<string, string>(header.Key, header.Value?.ToDelim(",")))
                                                                      ?? new List<KeyValuePair<string, string>>();

        _logger.LogDebug($"{headerVariables?.Count() ?? 0} response header variables loaded");

        return headerVariables;
    }

    private async Task<IEnumerable<KeyValuePair<string, string>>> GetResponseBodyVariables(HttpResponseMessage response, IOutboundRoute outboundRoute)
    {
        IEnumerable<KeyValuePair<string, string>> bodyVariables = new List<KeyValuePair<string, string>>();

        try
        {
            if (response.Content != null)
            {
                string body = await response.Content.ReadAsStringAsync();

                if (string.IsNullOrWhiteSpace(body) == false)
                {
                    _logger.LogDebug($"{nameof(outboundRoute)}.DeserializeResponse set to {outboundRoute.DeserializeResponse}");

                    if (outboundRoute.DeserializeResponse == true)
                    {
                        if (body.Substring(0, 1).SameAs("["))
                        {
                            var bodydictionaryCollection = JsonSerializer.Deserialize<IEnumerable<Dictionary<string, JsonElement>>>(body);

                            if (bodydictionaryCollection.Count() > 1)
                            {
                                throw new Exception("Request body is collection of items. Ensure InboundRoute.DeserializeRequest = 0 (false) and deserialize body variable in SQL");
                            }
                            else
                            {
                                bodyVariables = bodydictionaryCollection?.FirstOrDefault()?.Select(bd => new KeyValuePair<string, string>(bd.Key, bd.Value.GetRawText()?.Trim('"')))
                                            ?? new List<KeyValuePair<string, string>>();
                            }
                        }
                        else
                        {
                            var bodydictionary = JsonSerializer.Deserialize<Dictionary<string, JsonElement>>(body);

                            bodyVariables = bodydictionary?.Select(bd => new KeyValuePair<string, string>(bd.Key, bd.Value.GetRawText()?.Trim('"')))
                                            ?? new List<KeyValuePair<string, string>>();
                        }

                    }
                }

                bodyVariables = bodyVariables.Append(new KeyValuePair<string, string>("Body", body));
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, $"Failed to parse outbound response body to variables: {ex.Message}");
        }

        _logger.LogDebug($"{bodyVariables?.Count() ?? 0} response body variables loaded");

        return bodyVariables;
    }

}
