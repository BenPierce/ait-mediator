﻿using AIT.Mediator.Common.Core.Enums;
using AIT.Mediator.Common.Outbound.Abstractions;

namespace AIT.Mediator.API.Outbound.Models;

class OutboundRestRequest
{
    public readonly IOutboundRoute OutboundRoute;

    public string Url { get; set; }
    public RestType RestType { get; set; }
    public IDictionary<string, string> Headers { get; set; }
    public string Body { get; set; }

    public OutboundRestRequest(IOutboundRoute outboundRoute)
    {
        OutboundRoute = outboundRoute ?? throw new ArgumentNullException(nameof(outboundRoute));
    }
}
