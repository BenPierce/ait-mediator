using AIT.Crypto.Settings;
using System.Diagnostics;

namespace AIT.Mediator.API;

public class Program
{
    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.ConfigureAppConfiguration(config =>
                {
                    config.AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true);

                    if (Debugger.IsAttached)
                    {
                        config.AddJsonFile($"appsettings.Development.json", optional: true, reloadOnChange: false);
                    }
                    else
                    {
                        config.AddEncryptedJsonFile($"appsettings.enc.json", optional: false, reloadOnChange: false);
                    }
                });

                webBuilder.UseStartup<Startup>();
            });
}
