﻿using AIT.Mediator.API.Inbound.Services.PDF.Models;
using AIT.Mediator.Common.Core.Extensions;
using AIT.Mediator.Common.PDF.Abstractions;
using AIT.Mediator.Database.PDF;
using System.Diagnostics;

namespace AIT.Mediator.API.Inbound.Services.PDF;

public interface IPsrService
{
    Task<PsrResult> CreatePdf(string documentNo, string documentCode);        
}

class PsrService : IPsrService
{
    private readonly ILogger<PsrService> _logger;
    private readonly IPdfDataSource _dataSource;

    private string _netTraderExe => @"C:\Program Files (x86)\Auto-IT\Units 2\nettrader.exe";

    public PsrService(
        ILogger<PsrService> logger,
        IPdfDataSource dataSource
        )
    {
        _logger = logger;
        _dataSource = dataSource;
    }


    public Task<PsrResult> CreatePdf(string documentNo, string documentCode)
    {
        return Task.Run<PsrResult>(async () =>
        {
            try
            {
                if (string.IsNullOrWhiteSpace(documentNo)) throw new ArgumentException($"'{nameof(documentNo)}' cannot be null or whitespace.", nameof(documentNo));
                if (string.IsNullOrWhiteSpace(documentCode)) throw new ArgumentException($"'{nameof(documentCode)}' cannot be null or whitespace.", nameof(documentCode));
                if(documentCode.Length != 3) throw new ArgumentException($"'{nameof(documentCode)}' must be 3 characters");

                var timeoutSec = 60;

                var documentSettings = await _dataSource.GetPdfDocumentSettings(new(
                    documentNo: documentNo,
                    documentCode: documentCode
                    ));


                if (string.IsNullOrWhiteSpace(documentSettings?.EnvironmentName)) throw new Exception($"'{nameof(documentSettings.EnvironmentName)}' cannot be null or whitespace.");
                
                if (string.IsNullOrWhiteSpace(documentSettings?.OutputFilePath)) throw new Exception($"'{nameof(documentSettings.OutputFilePath)}' cannot be null or whitespace.");
                if (Directory.Exists(Path.GetDirectoryName(documentSettings.OutputFilePath)) != true) throw new DirectoryNotFoundException($"'{nameof(documentSettings.OutputFilePath)}' '{documentSettings.OutputFilePath}' does not exist or is not accessible");

                if (string.IsNullOrWhiteSpace(documentSettings?.TemplateFilePath)) throw new Exception($"'{nameof(documentSettings.TemplateFilePath)}' cannot be null or whitespace.");
                if (File.Exists(documentSettings.TemplateFilePath) != true) throw new FileNotFoundException($"'{nameof(documentSettings.TemplateFilePath)}' '{documentSettings.TemplateFilePath}' does not exist or is not accessible");

                if (string.IsNullOrWhiteSpace(documentSettings?.ParameterTypes)) throw new Exception($"'{nameof(documentSettings.ParameterTypes)}' cannot be null or whitespace.");
                if (string.IsNullOrWhiteSpace(documentSettings?.ParameterValues)) throw new Exception($"'{nameof(documentSettings.ParameterValues)}' cannot be null or whitespace.");

                var outputDirectory =  Path.GetDirectoryName(documentSettings.OutputFilePath);

                if (Directory.Exists(outputDirectory) != true) Directory.CreateDirectory(outputDirectory);

                var fileNameWithoutExtension = Guid.NewGuid().ToString();
                var outputFileName = Path.Combine(outputDirectory, $"{fileNameWithoutExtension}.pdf");

                if (File.Exists(outputFileName) != true)
                {
                    var webTraderArguments = BuildWebTraderArguments(
                        environmentName: documentSettings.EnvironmentName,
                        psrFilePath: documentSettings.TemplateFilePath,
                        outputFilePath: outputFileName,
                        parameterTypes: documentSettings.ParameterTypesAry,
                        parameterValues: documentSettings.ParameterValuesAry
                        );

                    var process = new Process();

                    process.StartInfo.FileName = _netTraderExe;
                    process.StartInfo.Arguments = webTraderArguments;
                    process.Start();

                    var timeoutMs = timeoutSec * 1000;


                    if (process.WaitForExit(timeoutMs))
                    {
                        if (File.Exists(outputFileName))
                        {
                            return new()
                            {
                                IsSuccess = true,
                                FilePath = outputFileName
                            };
                        }
                        else
                        {
                            LogFailedPdf(webTraderArguments, documentSettings, fileNameWithoutExtension);
                            throw new Exception("Failed to generate PDF");
                        }
                    }
                    else
                    {
                        throw new Exception($"Timeout out after {timeoutSec} seconds");
                    }
                }
                else
                {
                    return new()
                    {
                        IsSuccess = true,
                        FilePath = outputFileName
                    };
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{0} failed to create PDF: {1}", nameof(PsrService), ex.Message);

                return new()
                {
                    ErrorMessage = ex.Message
                };
            }
        });

    }

    private void LogFailedPdf(string arguments, IPdfDocumentSettings settings, string fileNameWithoutExtension)
    {
        try
        {
            var logFilePath = Path.Combine(settings.OutputFilePath, $"{fileNameWithoutExtension}.log");

            if (File.Exists(logFilePath))
            {
                var logMessage = File.ReadAllText(logFilePath);
                _logger.LogError($"Failed to generate PDF from PSR. Parameters: {arguments} {logMessage}");
            }
        }
        catch { }
    }

    private string BuildWebTraderArguments(string environmentName, string psrFilePath, string outputFilePath, string[] parameterTypes, string[] parameterValues)
    {
        var parameterValueStr = parameterValues?.Any() == true
            ? parameterValues.Select(p => string.IsNullOrWhiteSpace(p) ? "NULL" : p.Trim()).ToDelim("^^")
            : "";

        var parameterTypeStr = parameterTypes?.ToDelim("^^");


        return $"\";Action=PDFPrint;PSRFile={psrFilePath};DB={environmentName};ArgValues={parameterValueStr};Argtype={parameterTypeStr};OutputPDFfile={outputFilePath};\"";
    }




    //private (string[] parameterTypes, string[] parameterValues) GetDocumentParameters(string documentType, string documentNo, string branch)
    //{
    //    if (string.IsNullOrWhiteSpace(documentType)) throw new ArgumentException($"'{nameof(documentType)}' cannot be null or whitespace.", nameof(documentType));

    //    var documentTypeSanitized = documentType.ToUpper().Trim();

    //    return documentTypeSanitized switch
    //    {
    //        //"BDD" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //A_bank_deposit_detail_list.psr
    //        //"BDL" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //A_bank_deposit_summary_list.psr
    //        //"CHQ" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //A_cheque_print.psr
    //        //"CQR" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //A_cheque_remittance_print.psr
    //        "DRC" => (new string[] { "string" }, new string[] { documentNo }), //A_debtor_receipt.psr
    //        "DRI" => (new string[] { "string", "datetime" }, new string[] { documentNo, null }), //A_rental_invoice.psr
    //        //"DSP" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //A_debtor_sprint.psr
    //        "FAP" => (new string[] { "string" }, new string[] { documentNo }), //A_fas_purchase_order.psr
    //        "FAS" => (new string[] { "string" }, new string[] { documentNo }), //A_assetsale_invoice.psr
    //        //"GCQ" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //A_gl_cheque_printout.psr
    //        //"GCR" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //A_gl_cheque_remittance_print.psr
    //        "GDS" => (new string[] { "number" }, new string[] { documentNo }), //sroom_good_deal_sheet.psr
    //        //"IBT" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //P_parts_inter_branch_transfer.psr                             //Need to look at from/to branch
    //        //"JDA" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //R_jd_ag_rental_agreement.psr;R_jd_ind_rental_agreement.psr
    //        "LCA" => (new string[] { "number" }, new string[] { documentNo }), //W_loan_car_agreement.psr
    //        "LCR" => (new string[] { "number" }, new string[] { documentNo }), //W_loan_car_agreement.psr
    //        //"MAL" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //G_marketing_address_label.psr
    //        //"MDS" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //sroom_summary.psr
    //        //"MGE" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //MYGST03Form.psr
    //        //"PBN" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //STAT_NSW_form2.psr;STAT_NSW_form4.psr;STAT_NSW_form7.psr;STAT_NSW_form8.psr;STAT_NSW_form11.psr;STAT_NSW_form14.psr
    //        "PCN" => (new string[] { "string", "string" }, new string[] { "C", documentNo }), //P_parts_credit_note.psr
    //        //"PDL" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //g_part_delivery_labels.psr
    //        "PMI" => (new string[] { "string" }, new string[] { documentNo }), //a_promissory_note_interest_invoice.psr
    //        "PMN" => (new string[] { "stringlist" }, new string[] { "I", documentNo }), //a_promissory_note_invoice.psr
    //        //"PPL" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //G_price_label_standard_16.psr
    //        "PPO" => (new string[] { "number", "number", "string", "string", "string", "string" }, new string[] { documentNo, documentNo, "CPO", "S", "N", "Y" }), //P_parts_purchase_order.psr
    //        "PPR" => (new string[] { "string", "string" }, new string[] { documentNo, "N" }), //P_parts_purchase_return.psr
    //        //"PRP" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //A_payroll_payslip.psr
    //        //"PSB" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //P_picking_slip_by_branch.psr
    //        //"PSD" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //P_picking_slip_by_wks_reqdate.psr
    //        //"PSO" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //P_picking_slip_by_purord.psr
    //        //"PSP" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //P_picking_slip_by_order_number.psr
    //        //"PSR" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //P_picking_slip_by_wks_rono.psr
    //        "PUR" => (new string[] { "string", "string" }, new string[] { documentNo, "Y" }), //A_creditor_purchase_order.psr
    //        "RAI" => (new string[] { "stringlist" }, new string[] { documentNo }), //R_rental_adj_invoice.psr
    //        //"RC"  => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //r_rental_contract.psr
    //        "RIN" => (new string[] { "stringlist" }, new string[] { documentNo }), //R_rental_invoice.psr
    //        "RTQ" => (new string[] { "number" }, new string[] { documentNo }), //R_rental_quotation.psr
    //        "SAI" => (new string[] { "string" }, new string[] { documentNo }), //SINGLE=s_sales_invoice_singular.psr;MULTI=s_sales_invoice_exc_tax.psr,s_sales_invoice_inc_tax.psr
    //        "SCN" => (new string[] { "number" }, new string[] { documentNo }), //sroom_veh_sales_contract.psr
    //        "SCU" => (new string[] { "number" }, new string[] { documentNo }), //sroom_veh_sales_contract.psr
    //        "SIN" => (new string[] { "string" }, new string[] { documentNo }), //A_sundry_invoice.psr
    //        "SOI" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //P_parts_invoice.psr
    //        "SOO" => (new string[] { "number" }, new string[] { documentNo }), //P_parts_quotation.psr
    //        "SOQ" => (new string[] { "number" }, new string[] { documentNo }), //P_parts_quotation.psr
    //        "SOS" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //P_parts_invoice.psr

    //        //"SPA" => (new string[] { "number", "number", "string", "string", "datetime","string", "string", "string", "string", "string" }, new string[] { "1", "1", "CASHP-MB", "CASHP-MB", "1/13/2022","N","Y","Y", "1", "zzzzzzzz", "B" }), //A_debtor_sprint_as_at.psr

    //        "SRQ" => (new string[] { "number" }, new string[] { documentNo }), //sroom_veh_vosa_duplex.psr;sroom_delivery_checklist.psr;sroom_get_ready_sheet.psr
    //        "STS" => (new string[] { "number" }, new string[] { documentNo }), //s_stock_transfer_slip.psr
    //        "SUO" => (new string[] { "number" }, new string[] { documentNo }), //P_parts_internal_order.psr
    //        "SUP" => (new string[] { "string" }, new string[] { documentNo }), //s_suppl_sales_invoice_singular.psr
    //        "SWO" => (new string[] { "string" }, new string[] { documentNo }), //S_swap_out_invoice.psr
    //        //"TII" => (new string[] { "string", "number" }, new string[] { "", documentNo }), //S_trade_in_invoice.psr
    //        //"VHQ" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //VIC=sroom_regn_info_VIC.psr;TAS=sroom_regn_info_TAS.psr
    //        //"VRF" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //VIC=sroom_regn_form_VIC.psr;NSW=sroom_regn_form_NSW.psr;QLD=sroom_regn_form_QLD.psr;TAS=sroom_regn_form_TAS.psr;SA=sroom_regn_form_SA_new.psr;SA=sroom_regn_form_SA_used.psr;WA=sroom_regn_form_WA.psr
    //        //"VSF" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //STAT_SA_form1&2.psr;STAT_SA_form9.psr;STAT_WA_form4.psr;STAT_WA_form6.psr;STAT_WA_form7.psr;sroom_regn_form_VIC.psr;sroom_regn_form_NSW.psr;sroom_regn_form_QLD.psr;sroom_regn_form_TAS.psr;sroom_regn_form_SA_new.psr;sroom_regn_form_SA_used.psr;sroom_regn_form_WA.psr;sroom_transfer_form_VIC.psr;sroom_transfer_form_electronic_vic.psr;sroom_transfer_form_QLD.psr;sroom_transfer_form_TAS.psr;sroom_transfer_form_SA.psr
    //        //"VTF" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //VIC=sroom_transfer_form_VIC.psr;QLD=sroom_transfer_form_QLD.psr;TAS=sroom_transfer_form_TAS.psr;SA=sroom_transfer_form_SA.psr
    //        //"VTI" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //VIC=sroom_transfer_info_VIC.psr;TAS=sroom_transfer_info_TAS.psr
    //        "WCN" => (new string[] { "string", "string" }, new string[] { "C", documentNo }), //W_workshop_credit_note.psr
    //        "WFC" => (new string[] { "string", "string" }, new string[] { "C", documentNo }), //W_workshop_credit_note.psr
    //        "WFI" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //W_workshop_ro_invoice.psr
    //        "WFR" => (new string[] { "string", "number", "string" }, new string[] { branch, documentNo, "Y" }), //W_workshop_repair_order.psr
    //        "WPO" => (new string[] { "string", "string" }, new string[] { documentNo, "S" }), //W_workshop_purchase_order.psr
    //        "WQO" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //W_workshop_quotation.psr
    //        "WRE" => (new string[] { "string", "number", "string" }, new string[] { branch, documentNo, "Y" }), //W_workshop_repair_order.psr
    //        "WRI" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //W_workshop_ro_invoice.psr
    //        "WSI" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //W_workshop_parts_issue_internal.psr
    //        "WSR" => (new string[] { "string", "string" }, new string[] { "I", documentNo }), //W_workshop_parts_issue_retail.psr
    //        _ => throw new NotSupportedException($"Document type '{documentType}' is not supported")
    //    };

    //}

}
