﻿namespace AIT.Mediator.API.Inbound.Services.PDF.Models;

public class PsrResult
{
    public bool IsSuccess { get; init; }
    public string FilePath { get; init; }
    public string ErrorMessage { get; set; }
}
