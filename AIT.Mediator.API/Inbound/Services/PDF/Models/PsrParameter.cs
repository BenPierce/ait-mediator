﻿namespace AIT.Mediator.API.Inbound.Services.PDF.Models;

public class PsrParameter
{
    public string Name { get; set; }
    public string Type { get; set; }
}
