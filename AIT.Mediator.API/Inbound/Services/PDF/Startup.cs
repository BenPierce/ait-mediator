﻿namespace AIT.Mediator.API.Inbound.Services.PDF;

public static class Startup
{
    public static void AddPDF(this IServiceCollection services)
    {
        services.AddTransient<IPsrService, PsrService>();
    }
}