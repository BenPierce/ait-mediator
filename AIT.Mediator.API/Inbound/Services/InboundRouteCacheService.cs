﻿using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.Common.Inbound.PubSub;
using AIT.Mediator.Database.Inbound;
using AIT.PubSub;
using System.Collections.Concurrent;

namespace AIT.Mediator.API.Inbound.Services;

class InboundRouteCacheService :
    IHostedService,
    IDisposable,
    IHandler<GetRoutesRequest, GetRoutesResponse>,
    ISubscriber<InboundRouteCacheChangeMessage>
{
    private ILogger<InboundRouteCacheService> _logger;
    private readonly IInboundDataSource _dataSource;
    private readonly IPubSubSingletonService _pubSubSingletonService;

    public InboundRouteCacheService(
        ILogger<InboundRouteCacheService> logger,
        IInboundDataSource inboundDataSource,
        IPubSubSingletonService pubSubSingletonService
        )
    {
        _logger = logger;
        _dataSource = inboundDataSource;
        _pubSubSingletonService = pubSubSingletonService;

        _pubSubSingletonService.Subscribe(this);
    }


    private ConcurrentBag<IInboundRouteWithKeys> _inboundRouteCache { get; set; }

    private IEnumerable<IInboundRouteWithKeys> _inboundRoutes => _inboundRouteCache?.ToList();

    private System.Timers.Timer _timer;

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        try
        {
            _logger.LogInformation("Starting Inbound Route Cache Service");

            await RefreshCache();

            _timer = new System.Timers.Timer(1000 * 60 * 60);
            _timer.AutoReset = true;
            _timer.Elapsed += TimerElapsed;

            _logger.LogInformation($"Inbound Route Cache Service started");
        }
        catch (Exception ex)
        {
            _logger.LogCritical(ex, "Failed to start Inbound Route Cache Service");
        }
    }

    

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        _logger?.LogInformation($"Inbound Route Cache Service stopping");
    }

    public void Dispose()
    {
        try
        {
            _timer.Elapsed -= TimerElapsed;
            _pubSubSingletonService.Unsubscribe(this);
        }
        catch { }
    }

    public async Task<GetRoutesResponse> OnRequest(GetRoutesRequest request)
    {
        return new GetRoutesResponse
        {
            RouteWithKeys = _inboundRoutes
        };
    }

    public async Task OnEvent(InboundRouteCacheChangeMessage message)
    {
        try
        {
            _logger.LogInformation("Cache change notification received, updating cache");

            await RefreshCache();

            _logger.LogInformation("Cache updated successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, $"Failed to process cache update: {ex.Message}");
        }

    }

    private bool _isRebuilding;
    private async Task RefreshCache()
    {
        if (_isRebuilding)
        {
            _logger.LogInformation("Inbound cache rebuild triggered, but rebuild already running");
            return;
        }
        try
        {
            _isRebuilding = true;

            _logger.LogInformation("Inbound cache rebuild commenced");

            var inboundRoutes = await _dataSource.GetInboundRouteWithKeys();

            if (inboundRoutes?.Any() != true)
            {
                throw new Exception("No inbound routes retrieved");
            }

            _inboundRouteCache = new ConcurrentBag<IInboundRouteWithKeys>(inboundRoutes);

            _logger.LogInformation("Cache rebuild complete with {0} routes", inboundRoutes.Count());
        }
        catch
        {
            throw;
        }
        finally
        {
            _isRebuilding = false;
        }
    }

    private async void TimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
    {
        try
        {
            _logger.LogInformation("Automated inbound cache rebuild triggered");

            await RefreshCache();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Failed to rebuild Inbound Route Cache: {0}", ex.Message);
        }
    }

}
