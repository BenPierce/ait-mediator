﻿using AIT.Mediator.API.Core.HttpHelper;
using AIT.Mediator.API.Inbound.Services.PDF;
using AIT.Mediator.Common.Core.Extensions;
using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.Common.Inbound.PubSub;
using AIT.Mediator.Database.Inbound;
using AIT.PubSub;
using Microsoft.Extensions.Primitives;

namespace AIT.Mediator.API.Inbound.Services;

class InboundRequestService : IInboundRequestService
{
    private readonly ILogger<InboundRequestService> _logger;
    private readonly IPubSubSingletonService _pubSubSingletonService;
    private readonly IInboundDataSource _dataSource;
    private readonly IPsrService _psrService;

    public InboundRequestService(
        ILogger<InboundRequestService> logger,
        IPubSubSingletonService pubSubSingletonService,
        IInboundDataSource dataSource,
        IPsrService psrService
        )
    {
        _logger = logger;
        _pubSubSingletonService = pubSubSingletonService;
        _dataSource = dataSource;
        _psrService = psrService;
    }

    public void Dispose()
    {

    }

    public async Task ProcessRequest(HttpContext context)
    {
        try
        {
            HttpRequest request = context.Request;

            _logger.LogDebug($"Request Path: {request.Path}");

            var appKey = GetAPIKey(request) 
                ?? GetBasicAuth(request);

            var route = await GetMatchedRoute(
                path: request.Path,
                appKey: appKey
                );

            if (route is null)
            {
                _logger.LogError($"Failed to find matching route");
                context.Response.StatusCode = HttpStatusCode.Status404NotFound;
            }
            else if (route.IsDocumentRequest)
            {                
                _logger.LogDebug($"Document request route identified: {route.Name} '{route.UrlMask}'");

                IDictionary<string, object> parameters = await GetVariables(request, route);

                _logger.LogDebug($"Available parameters are: {JsonSerializer.Serialize(parameters)}");

                var document = await _dataSource.GetPdfDocument(
                    query: route.Sql,
                    parameters: parameters
                    );

                if (string.IsNullOrWhiteSpace(document.DocumentNo))
                {
                    _logger.LogWarning($"Failed to process document request: DocumentNo was not provided");
                    context.Response.StatusCode = HttpStatusCode.Status400BadRequest;

                }
                else if (string.IsNullOrWhiteSpace(document.DocumentCode) && string.IsNullOrWhiteSpace(document.DocumentType))
                {
                    _logger.LogWarning($"Failed to process document request: DocumentCode was not provided");
                    context.Response.StatusCode = HttpStatusCode.Status400BadRequest;
                }
                else
                {
                    var pdfResult= await _psrService.CreatePdf(document.DocumentNo, document.DocumentCode ?? document.DocumentType);

                    if (pdfResult.IsSuccess != true)
                    {
                        throw new Exception($"Failed to create PDF: {pdfResult.ErrorMessage}");
                    }


                    context.Response.ContentType = HttpContentType.PDF;
                    context.Response.StatusCode = HttpStatusCode.Status200OK;

                    using (var fs = new FileStream(pdfResult.FilePath, FileMode.Open))
                    {
                        await fs.CopyToAsync(context.Response.Body);
                    }
                }
            }
            else
            {
                _logger.LogDebug($"Optimal route identified: {route.Name} '{route.UrlMask}'");

                IDictionary<string, object> parameters = await GetVariables(request, route);

                _logger.LogDebug($"Available parameters are: {JsonSerializer.Serialize(parameters)}");

                _logger.LogDebug($"Processing SQL");

                string responseBody = await _dataSource.GetInboundRouteData(
                    query: route.Sql,
                    parameters: parameters,
                    serialize: route.SerializeResponse
                    );

                _logger.LogDebug($"SQL processed");

                context.Response.StatusCode = HttpStatusCode.Status200OK;

                if (string.IsNullOrWhiteSpace(responseBody) == false)
                {
                    _logger.LogDebug($"Writing response body");
                    context.Response.ContentType = HttpContentType.JSON;
                    await context.Response.WriteAsync(responseBody);
                }

                _logger.LogInformation($"Sending Response");
            }


        }
        catch (Exception ex)
        {
            _logger.LogError(ex, $"Failed to process inbound request");
            context.Response.StatusCode = HttpStatusCode.Status500InternalServerError;
        }
    }

    private string GetKey(HttpRequest request, string key)
    {
        _logger.LogDebug($"Reading {key} from headers");

        StringValues keyValue;
        request.Headers.TryGetValue(key, out keyValue);

        _logger.LogDebug($"{key} key read");

        return keyValue;
    }

    private string GetAPIKey(HttpRequest request) => GetKey(request, @"api_key");

    private string GetBasicAuth(HttpRequest request)
    {
        var value = GetKey(request, @"authorization");
        if (string.IsNullOrWhiteSpace(value) != true && value.StartsWith("basic ",StringComparison.OrdinalIgnoreCase)) 
        {
            value = value.Substring(6);
        }

        return value;

    }



    private async Task<IInboundRouteWithKeys> GetMatchedRoute(string path, string appKey)
    {
        _logger.LogDebug("Finding matching inbound route");

        var getRoutesResponse = await _pubSubSingletonService.Request<GetRoutesRequest, GetRoutesResponse>(new GetRoutesRequest());

        var matchedRoute = getRoutesResponse?.RouteWithKeys
            ?.Where(r => r.IsValidAppKey(appKey) && r.IsMaskMatch(path))
            ?.OrderBy(r => r.MaskVariableCount)
            ?.FirstOrDefault();

        return matchedRoute;
    }

    private async Task<IDictionary<string, object>> GetVariables(HttpRequest request, IInboundRouteWithKeys route)
    {
        IEnumerable<KeyValuePair<string, string>> headerVariables = GetHeaderVariables(request);

        IEnumerable<KeyValuePair<string, string>> routeVarables = GetRouteVariables(route, request.Path);

        IEnumerable<KeyValuePair<string, string>> parameterVarables = GetParameterVariables(request);

        IEnumerable<KeyValuePair<string, string>> bodyVariables = await GetBodyVariables(request, route);

        return headerVariables.Concat(routeVarables)
                              .Concat(parameterVarables)
                              .Concat(bodyVariables)
                              .GroupBy(d => d.Key)
                              .ToDictionary(d => d.Key, d => (object)d.First().Value);
    }

    private bool IsVariable(string urlPart) => Regex.IsMatch(urlPart, @"{.*?}");

    private IEnumerable<KeyValuePair<string, string>> GetHeaderVariables(HttpRequest request)
    {
        var headerVariables = request.Headers.Select(header => new KeyValuePair<string, string>(header.Key, header.Value.ToSingleString()))
                                                                      ?? new List<KeyValuePair<string, string>>();

        _logger.LogDebug($"{headerVariables?.Count() ?? 0} header variables loaded");

        return headerVariables;
    }

    private IEnumerable<KeyValuePair<string, string>> GetRouteVariables(IInboundRouteWithKeys inboundRoute, string baseUrl)
    {
        if (string.IsNullOrWhiteSpace(baseUrl)) throw new ArgumentNullException(nameof(baseUrl));

        IEnumerable<KeyValuePair<string, string>> routeVariables = null;
        string[] urlMaskParts = inboundRoute.UrlMask.Split('/').ToArray();
        string[] urlParts = baseUrl.Split('/').ToArray();

        if (urlMaskParts.Length != urlParts.Length)
        {
            throw new Exception("Route has a different number of slashes then mask");
        }
        else
        {
            var results = new List<KeyValuePair<string, string>>();

            for (int i = 0; i < urlMaskParts.Length; i++)
            {
                if (IsVariable(urlMaskParts[i]))
                {
                    string variable = urlMaskParts[i].Trim('{').Trim('}');
                    string value = urlParts[i];

                    results.Add(new KeyValuePair<string, string>(variable, value));
                }
            }

            routeVariables = results ?? new List<KeyValuePair<string, string>>();
        }

        _logger.LogDebug($"{routeVariables?.Count() ?? 0} route variables loaded");

        return routeVariables;
    }

    private IEnumerable<KeyValuePair<string, string>> GetParameterVariables(HttpRequest request)
    {
        var parameterVariables = request.Query?.Select(query => new KeyValuePair<string, string>(query.Key, query.Value.ToSingleString()))
                                                                        ?? new List<KeyValuePair<string, string>>();

        _logger.LogDebug($"{parameterVariables?.Count() ?? 0} url parameter variables loaded");

        return parameterVariables;
    }

    private async Task<IEnumerable<KeyValuePair<string, string>>> GetBodyVariables(HttpRequest request, IInboundRouteWithKeys route)
    {
        IEnumerable<KeyValuePair<string, string>> bodyVariables = new List<KeyValuePair<string, string>>();

        try
        {
            if (request.Body != null)
            {
                var reader = new StreamReader(request.Body);

                string body = await reader.ReadToEndAsync();

                if (string.IsNullOrWhiteSpace(body) == false)
                {
                    _logger.LogDebug($"InboundRoute.DeserializeRequest set to {route.DeserializeRequest}");

                    if (route.DeserializeRequest == true)
                    {
                        if (request.ContentType.SameAs(HttpContentType.JSON))
                        {
                            if (body.Substring(0, 1).SameAs("["))
                            {
                                throw new Exception("Request body is collection of items. Ensure InboundRoute.DeserializeRequest = 0 (false) and deserialize body variable in SQL");
                            }
                            else
                            {
                                var bodydictionary = JsonSerializer.Deserialize<Dictionary<string, JsonElement>>(body);

                                bodyVariables = bodydictionary?.Select(bd => {

                                    var key = bd.Key;
                                    var value =  bd.Value.ValueKind == JsonValueKind.Null ? null : bd.Value.GetRawText()?.Trim('"');

                                    return new KeyValuePair<string, string>(key, value);
                                    })
                                ?? new List<KeyValuePair<string, string>>();
                            }
                        }
                        else
                        {
                            throw new Exception($"Unsupported Content-Type '{request.ContentType}'");
                        }
                    }
                }

                bodyVariables = bodyVariables.Append(new KeyValuePair<string, string>("Body", body));
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, $"Failed to parse inboud request body to variables: {ex.Message}");
        }

        _logger.LogDebug($"{bodyVariables?.Count() ?? 0} body variables loaded");

        return bodyVariables;
    }
}
