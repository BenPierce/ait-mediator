﻿using AIT.IOC.Factories;
using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.API.Core.HttpHelper;

namespace AIT.Mediator.API.Inbound;

static class RoutingEntryPoint
{
    public static void InboundRoute(this IApplicationBuilder app)
    {
        app.Run(async context =>
        {
            var serviceProvider = context.RequestServices;

            var _logger = serviceProvider.GetRequiredService<ILogger<Program>>();

            try
            {
                _logger.LogInformation("Request received");

                var inboundRequestServiceFactory = serviceProvider.GetRequiredService<IFactory<IInboundRequestService>>();

                using (var inboundRequestService = inboundRequestServiceFactory.Build())
                {
                    await inboundRequestService.ProcessRequest(context);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Request error: {ex.Message}");
                context.Response.StatusCode = HttpStatusCode.Status500InternalServerError;
            }
        });
    }

}

