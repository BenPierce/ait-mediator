﻿using Microsoft.AspNetCore.Mvc;

namespace AIT.Mediator.API.Core.HttpHelper;

public static class HttpStatus
{
    public static StatusCodeResult Status100Continue => new StatusCodeResult(100);
    public static StatusCodeResult Status101SwitchingProtocols => new StatusCodeResult(101);
    public static StatusCodeResult Status102Processing => new StatusCodeResult(102);

    public static StatusCodeResult Status200OK => new StatusCodeResult(200);
    public static StatusCodeResult Status201Created => new StatusCodeResult(201);
    public static StatusCodeResult Status202Accepted => new StatusCodeResult(202);
    public static StatusCodeResult Status203NonAuthoritative => new StatusCodeResult(203);
    public static StatusCodeResult Status204NoContent => new StatusCodeResult(204);
    public static StatusCodeResult Status205ResetContent => new StatusCodeResult(205);
    public static StatusCodeResult Status206PartialContent => new StatusCodeResult(206);
    public static StatusCodeResult Status207MultiStatus => new StatusCodeResult(207);
    public static StatusCodeResult Status208AlreadyReported => new StatusCodeResult(208);
    public static StatusCodeResult Status226IMUsed => new StatusCodeResult(226);

    public static StatusCodeResult Status300MultipleChoices => new StatusCodeResult(300);
    public static StatusCodeResult Status301MovedPermanently => new StatusCodeResult(301);
    public static StatusCodeResult Status302Found => new StatusCodeResult(302);
    public static StatusCodeResult Status303SeeOther => new StatusCodeResult(303);
    public static StatusCodeResult Status304NotModified => new StatusCodeResult(304);
    public static StatusCodeResult Status305UseProxy => new StatusCodeResult(305);
    public static StatusCodeResult Status306SwitchProxy => new StatusCodeResult(306); // RFC 2616, removed
    public static StatusCodeResult Status307TemporaryRedirect => new StatusCodeResult(307);
    public static StatusCodeResult Status308PermanentRedirect => new StatusCodeResult(308);

    public static StatusCodeResult Status400BadRequest => new StatusCodeResult(400);
    public static StatusCodeResult Status401Unauthorized => new StatusCodeResult(401);
    public static StatusCodeResult Status402PaymentRequired => new StatusCodeResult(402);
    public static StatusCodeResult Status403Forbidden => new StatusCodeResult(403);
    public static StatusCodeResult Status404NotFound => new StatusCodeResult(404);
    public static StatusCodeResult Status405MethodNotAllowed => new StatusCodeResult(405);
    public static StatusCodeResult Status406NotAcceptable => new StatusCodeResult(406);
    public static StatusCodeResult Status407ProxyAuthenticationRequired => new StatusCodeResult(407);
    public static StatusCodeResult Status408RequestTimeout => new StatusCodeResult(408);
    public static StatusCodeResult Status409Conflict => new StatusCodeResult(409);
    public static StatusCodeResult Status410Gone => new StatusCodeResult(410);
    public static StatusCodeResult Status411LengthRequired => new StatusCodeResult(411);
    public static StatusCodeResult Status412PreconditionFailed => new StatusCodeResult(412);
    public static StatusCodeResult Status413RequestEntityTooLarge => new StatusCodeResult(413); // RFC 2616, renamed
    public static StatusCodeResult Status413PayloadTooLarge => new StatusCodeResult(413); // RFC 7231
    public static StatusCodeResult Status414RequestUriTooLong => new StatusCodeResult(414); // RFC 2616, renamed
    public static StatusCodeResult Status414UriTooLong => new StatusCodeResult(414); // RFC 7231
    public static StatusCodeResult Status415UnsupportedMediaType => new StatusCodeResult(415);
    public static StatusCodeResult Status416RequestedRangeNotSatisfiable => new StatusCodeResult(416); // RFC 2616, renamed
    public static StatusCodeResult Status416RangeNotSatisfiable => new StatusCodeResult(416); // RFC 7233
    public static StatusCodeResult Status417ExpectationFailed => new StatusCodeResult(417);
    public static StatusCodeResult Status418ImATeapot => new StatusCodeResult(418);
    public static StatusCodeResult Status419AuthenticationTimeout => new StatusCodeResult(419); // Not defined in any RFC
    public static StatusCodeResult Status421MisdirectedRequest => new StatusCodeResult(421);
    public static StatusCodeResult Status422UnprocessableEntity => new StatusCodeResult(422);
    public static StatusCodeResult Status423Locked => new StatusCodeResult(423);
    public static StatusCodeResult Status424FailedDependency => new StatusCodeResult(424);
    public static StatusCodeResult Status426UpgradeRequired => new StatusCodeResult(426);
    public static StatusCodeResult Status428PreconditionRequired => new StatusCodeResult(428);
    public static StatusCodeResult Status429TooManyRequests => new StatusCodeResult(429);
    public static StatusCodeResult Status431RequestHeaderFieldsTooLarge => new StatusCodeResult(431);
    public static StatusCodeResult Status451UnavailableForLegalReasons => new StatusCodeResult(451);

    public static StatusCodeResult Status500InternalServerError => new StatusCodeResult(500);
    public static StatusCodeResult Status501NotImplemented => new StatusCodeResult(501);
    public static StatusCodeResult Status502BadGateway => new StatusCodeResult(502);
    public static StatusCodeResult Status503ServiceUnavailable => new StatusCodeResult(503);
    public static StatusCodeResult Status504GatewayTimeout => new StatusCodeResult(504);
    public static StatusCodeResult Status505HttpVersionNotsupported => new StatusCodeResult(505);
    public static StatusCodeResult Status506VariantAlsoNegotiates => new StatusCodeResult(506);
    public static StatusCodeResult Status507InsufficientStorage => new StatusCodeResult(507);
    public static StatusCodeResult Status508LoopDetected => new StatusCodeResult(508);
    public static StatusCodeResult Status510NotExtended => new StatusCodeResult(510);
    public static StatusCodeResult Status511NetworkAuthenticationRequired => new StatusCodeResult(511);
}
