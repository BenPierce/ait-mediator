﻿namespace AIT.Mediator.API.Core.HttpHelper;

public static class HttpContentType
{
    public const string JSON = @"application/JSON";
    public const string XML = @"application/xml";
    public const string Text = @"text/plain";
    public const string PDF = @"application/pdf";
}

