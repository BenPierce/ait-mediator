﻿using AIT.Database;
using AIT.IOC.Factories;
using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.Common.PDF.Abstractions;
using AIT.Mediator.Database.Inbound.Queries;
using System.Data;
using System.Text.Json;

namespace AIT.Mediator.Database.Inbound;

public interface IInboundDataSource
{
    Task<IPdfDocument> GetPdfDocument(string query, IDictionary<string, object> parameters);
    Task<IInboundRoute> GetInboundRoute(GetInboundRoute.Parameter parameter);
    Task<IEnumerable<IInboundRoute>> GetInboundRoutes();
    Task<IEnumerable<IInboundRouteWithKeys>> GetInboundRouteWithKeys();

    Task<IInboundRoute> UpsertInboundRoute(UpsertInboundRoute.Parameter parameter, IEnumerable<int> validAppIds);

    Task<string> GetInboundRouteData(string query, IDictionary<string, object> parameters, bool serialize = false);
    Task DeleteInboundRoute(DeleteInboundRoute.Parameter parameter);
    Task<IEnumerable<IInboundRouteAppAccess>> GetInboundRouteAppAccess(GetInboundRouteAppAccess.Parameter parameter);

    Task<IEnumerable<IApp>> GetApps();
    Task<IApp> UpsertApp(UpsertApp.Parameter parameter);
    Task DeleteApp(DeleteApp.Parameter parameter);
}


class InboundDataSource : IInboundDataSource
{
    private readonly IFactory<IDbConnection> _dbConnectionFactory;

    public InboundDataSource(
        IFactory<IDbConnection> dbConnectionFactory
        )
    {
        _dbConnectionFactory = dbConnectionFactory;
    }

    private IDbConnection BuildConnection() => _dbConnectionFactory.Build();
    

    public async Task<IInboundRoute> GetInboundRoute(GetInboundRoute.Parameter parameter)
    {
        using (var connection = BuildConnection())
        {
            using (var proc = new GetInboundRoute())
            {
                return (await proc.ExecuteAsync(connection, parameter))?.FirstOrDefault();
            }
        }
    }

    public async Task<IEnumerable<IInboundRoute>> GetInboundRoutes()
    {
        using (var connection = BuildConnection())
        {
            using (var proc = new GetInboundRoutes())
            {
                return await proc.ExecuteAsync(connection);
            }
        }
    }

    public async Task<IEnumerable<IInboundRouteWithKeys>> GetInboundRouteWithKeys()
    {
        using (var connection = BuildConnection())
        {
            using (var proc = new GetInboundRouteWithKeys())
            {
                return await proc.ExecuteAsync(connection);
            }
        }
    }



    public async Task<IPdfDocument> GetPdfDocument(string query,IDictionary<string, object> parameters)
    {
        using (var connection = _dbConnectionFactory.Build())
        {
            return await connection.SelectOneAsync<PdfDocument>(
                query: query,
                parameters: parameters,
                isStoredProc: false
                );
        }
    }

    private class PdfDocument : IPdfDocument
    {
        public string DocumentNo { get; set; }
        public string DocumentCode { get; set; }
        public string DocumentType { get; set; }
    }


    public async Task<string> GetInboundRouteData(
        string query, 
        IDictionary<string,object> parameters,
        bool serialize = false
        )
    {
        if (serialize)
        {
            using (var connection = _dbConnectionFactory.Build())
            {
                var response =  await connection.SelectManyAsync<object>(
                        query: query,
                        parameters: parameters,
                        isStoredProc: false
                   );

                return JsonSerializer.Serialize(response);
            }
        }
        else
        {
            using (var connection = _dbConnectionFactory.Build())
            {
                return await connection.SelectOneAsync<string>(
                    query: query,
                    parameters: parameters,
                    isStoredProc: false
                    );
            }
        }
    }

    public async Task<IInboundRoute> UpsertInboundRoute(UpsertInboundRoute.Parameter parameter, IEnumerable<int> validAppIds)
    {
        IInboundRoute updatedRoute = null;

        using (var connection = _dbConnectionFactory.Build())
        {
            connection.Open();

            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    using var upsertInboundRouteProc = new UpsertInboundRoute();
                    
                    var routeId = await upsertInboundRouteProc.ExecuteAsync(connection, parameter, transaction);

                    using (var proc = new DeleteInboundRouteAppAccess())
                    {
                        await proc.ExecuteAsync(connection, new(routeId), transaction);
                    }

                    if (validAppIds?.Any() == true)
                    {
                        foreach (var validAppId in validAppIds)
                        {
                            using (var proc = new InsertInboundRouteAppAccess())
                            {
                                await proc.ExecuteAsync(connection, new(routeId, validAppId), transaction);
                            }
                        }
                    }

                    using (var proc = new GetInboundRoute())
                    {
                        updatedRoute = (await proc.ExecuteAsync(connection, new(routeId), transaction))?.FirstOrDefault()
                            ?? throw new Exception("Failed to retrieve upserted route"); 
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    connection.Close();
                    throw;
                }
            }

            connection.Close();
        }

        return updatedRoute;
    }

    

    public async Task DeleteInboundRoute(DeleteInboundRoute.Parameter parameter)
    {
        using (var connection = BuildConnection())
        {
            using (var proc = new DeleteInboundRoute())
            {
                await proc.ExecuteAsync(connection, parameter);
            }
        }
    }

    public async Task<IEnumerable<IInboundRouteAppAccess>> GetInboundRouteAppAccess(GetInboundRouteAppAccess.Parameter parameter)
    {
        using (var connection = BuildConnection())
        {
            using (var proc = new GetInboundRouteAppAccess())
            {
                return await proc.ExecuteAsync(connection,parameter);
            }
        }
    }

    public async Task<IEnumerable<IApp>> GetApps()
    {
        using (var connection = BuildConnection())
        {
            using (var proc = new GetApps())
            {
                return await proc.ExecuteAsync(connection);
            }
        }
    }

    public async Task<IApp> UpsertApp(UpsertApp.Parameter parameter)
    {
        using (var connection = BuildConnection())
        {
            using (var proc = new UpsertApp())
            {
                return await proc.ExecuteAsync(connection, parameter);
            }
        }
    }

    public async Task DeleteApp(DeleteApp.Parameter parameter)
    {
        using (var connection = BuildConnection())
        {
            using (var proc = new DeleteApp())
            {
                await proc.ExecuteAsync(connection, parameter);
            }
        }
    }


}
