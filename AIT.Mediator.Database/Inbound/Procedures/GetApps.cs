﻿using AIT.Database.Procedures;
using AIT.Mediator.Common.Inbound.Abstractions;

namespace AIT.Mediator.Database.Inbound.Queries;

public class GetApps : Procedure
    .WithoutParameters
    .WithResults<GetApps.Result>
{
    public override string Name => @"administrator.usp_Mediator_Inbound_GetApps";

    public class Result : IApp
    {
        public int? AppId { get; set; }
        public string Name { get; set; }
        public string AppKeyEnc { get; set; }
        public bool IsDisabled { get; set; }

        public int? Id => AppId;
    }
}
