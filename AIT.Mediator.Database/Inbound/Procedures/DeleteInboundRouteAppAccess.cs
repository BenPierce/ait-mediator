﻿using AIT.Database.Procedures;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Inbound.Queries;

public class DeleteInboundRouteAppAccess : Procedure
    .WithParameters<DeleteInboundRouteAppAccess.Parameter>
    .WithoutResults
{
    public override string Name => @"administrator.usp_Mediator_Inbound_DeleteRouteAppAccess";

    public class Parameter : ProcedureParameterBase
    {
        public int in_RouteId { get; init; }

        public Parameter(
            int routeId
            )
        {
            in_RouteId = Set(
               value: routeId,
               fieldName: nameof(routeId),
               isZeroAllowed: false,
               isNegativeAllowed: false
               );

        }
    }
}
