﻿using AIT.Database.Procedures;
using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Inbound.Queries;

public class GetInboundRouteAppAccess : Procedure
    .WithParameters<GetInboundRouteAppAccess.Parameter>
    .WithResults<GetInboundRouteAppAccess.Result>
{
    public override string Name => @"administrator.usp_Mediator_Inbound_GetRouteAppAccess";

    public class Parameter : ProcedureParameterBase
    {
        public int in_RouteId { get; init; }

        public Parameter(
            int routeId
            )
        {
            in_RouteId = Set(
               value: routeId,
               fieldName: nameof(routeId),
               isZeroAllowed: false,
               isNegativeAllowed: false
               );
        }
    }

    public class Result : IInboundRouteAppAccess
    {
        public int? AccessId { get; set; }
        public int AppId { get; set; }
        public int RouteId { get; set; }
        public string AppName { get; set; }
        public bool IsAccessGranted { get; set; }
        public bool IsAppDisabled { get; set; }
    }
}
