﻿using AIT.Database.Procedures;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Inbound.Queries;

public class DeleteApp : Procedure
    .WithParameters<DeleteApp.Parameter>
    .WithoutResults
{
    public override string Name => @"administrator.usp_Mediator_Inbound_DeleteApp";

    public class Parameter : ProcedureParameterBase
    {
        public int in_AppId { get; init; }

        public Parameter(
            int? appId
            )
        {
            if (appId == null) throw new Exception("Cannot delete app with null AppId");

            in_AppId = Set(
               value: (int)appId,
               fieldName: nameof(appId),
               isZeroAllowed: false,
               isNegativeAllowed: false
               );

        }
    }


}
