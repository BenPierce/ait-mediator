﻿using AIT.Database.Procedures;
using AIT.Mediator.Common.Inbound.Abstractions;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Inbound.Queries;

public class UpsertInboundRoute : Procedure
    .WithParameters<UpsertInboundRoute.Parameter>
    .WithSingleResult<int>
{
    public override string Name => @"administrator.usp_Mediator_Inbound_UpsertRoute";

    public class Parameter : ProcedureParameterBase
    {
        public int? in_RouteId { get; private set; }
        public string in_Name { get; private set; }
        public string in_UrlMask { get; private set; }
        public string in_Sql { get; private set; }
        public bool in_DeserializeRequest { get; private set; }
        public bool in_SerializeResponse { get; private set; }
        public bool in_IsDocumentRequest { get; private set; }

        public Parameter(
            int? routeId,
            string name,
            string urlMask,
            string sql,
            bool deserializeRequest,
            bool serializeResponse,
            bool isDocumentRequest
            )
        {
            in_RouteId = Set(
               value: routeId,
               fieldName: nameof(routeId),
               isZeroAllowed: false,
               isNegativeAllowed: false
               );

            in_Name = Set(
                value: name,
                fieldName: nameof(name),
                isNullable: false,
                maxLength: 255,
                isUpperCase: false
                );

            in_UrlMask = Set(
                value: urlMask,
                fieldName: nameof(urlMask),
                isNullable: false,
                maxLength: null,
                isUpperCase: false
                );

            in_Sql = Set(
                value: sql,
                fieldName: nameof(sql),
                isNullable: false,
                maxLength: null,
                isUpperCase: false
                );

            in_DeserializeRequest = Set(
                value: deserializeRequest,
                fieldName: nameof(deserializeRequest)
                );

            in_SerializeResponse = Set(
                value: serializeResponse,
                fieldName: nameof(serializeResponse)
                );

            in_IsDocumentRequest = Set(
                value: isDocumentRequest,
                fieldName: nameof(isDocumentRequest)
                );
        }

        public Parameter(
            IInboundRoute route
            )
        {
            in_RouteId = Set(
               value: route.RouteId,
               fieldName: nameof(route.RouteId),
               isZeroAllowed: false,
               isNegativeAllowed: false
               );

            in_Name = Set(
                value: route.Name,
                fieldName: nameof(route.Name),
                isNullable: false,
                maxLength: 255,
                isUpperCase: false
                );

            in_UrlMask = Set(
                value: route.UrlMask,
                fieldName: nameof(route.UrlMask),
                isNullable: false,
                maxLength: null,
                isUpperCase: false
                );

            in_Sql = Set(
                value: route.Sql,
                fieldName: nameof(route.Sql),
                isNullable: false,
                maxLength: null,
                isUpperCase: false
                );

            in_DeserializeRequest = Set(
                value: route.DeserializeRequest,
                fieldName: nameof(route.DeserializeRequest)
                );

            in_SerializeResponse = Set(
                value: route.SerializeResponse,
                fieldName: nameof(route.SerializeResponse)
                );

            in_IsDocumentRequest = Set(
                value: route.IsDocumentRequest,
                fieldName: nameof(route.IsDocumentRequest)
                );
        }
    }

}
