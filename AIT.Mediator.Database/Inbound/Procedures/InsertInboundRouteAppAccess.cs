﻿using AIT.Database.Procedures;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Inbound.Queries;

public class InsertInboundRouteAppAccess : Procedure
    .WithParameters<InsertInboundRouteAppAccess.Parameter>
    .WithoutResults
{
    public override string Name => @"administrator.usp_Mediator_Inbound_InsertRouteAppAccess";

    public class Parameter : ProcedureParameterBase
    {
        public int in_RouteId { get; init; }
        public int in_AppId { get; init; }

        public Parameter(
            int routeId,
            int appId
            )
        {
            in_RouteId = Set(
               value: routeId,
               fieldName: nameof(routeId),
               isZeroAllowed: false,
               isNegativeAllowed: false
               );

            in_AppId = Set(
               value: appId,
               fieldName: nameof(appId),
               isZeroAllowed: false,
               isNegativeAllowed: false
               );
        }
    }
}
