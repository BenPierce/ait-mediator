﻿using AIT.Database.Procedures;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Inbound.Queries;

public class DeleteInboundRoute : Procedure
    .WithParameters<DeleteInboundRoute.Parameter>
    .WithoutResults
{
    public override string Name => @"administrator.usp_Mediator_Inbound_DeleteRoute";

    public class Parameter : ProcedureParameterBase
    {
        public int in_RouteId { get; init; }

        public Parameter(
            int? routeId
            )
        {
            if (routeId == null) throw new Exception("Cannot delete route with null RouteId");

            in_RouteId = Set(
               value: (int)routeId,
               fieldName: nameof(routeId),
               isZeroAllowed: false,
               isNegativeAllowed: false
               );

        }
    }
}
