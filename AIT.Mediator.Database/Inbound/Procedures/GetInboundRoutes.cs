﻿using AIT.Database.Procedures;
using AIT.Mediator.Common.Inbound.Abstractions;

namespace AIT.Mediator.Database.Inbound.Queries;

public class GetInboundRoutes : Procedure
    .WithoutParameters
    .WithJsonResults<GetInboundRoutes.Result>
{
    public override string Name => @"administrator.usp_Mediator_Inbound_GetRoutes";

    public class Result : IInboundRoute
    {
        public int? RouteId { get; set; }
        public string Name { get; set; }
        public string UrlMask { get; set; }
        public string Sql { get; set; }
        public bool DeserializeRequest { get; set; }
        public bool SerializeResponse { get; set; }
        public bool IsDocumentRequest { get; set; }

        public int? Id => RouteId;

        public IEnumerable<InboundRouteAppAccessResult> AppAccessDMs { get; set; }

        public IEnumerable<IInboundRouteAppAccess> AppAccess => AppAccessDMs;

        public class InboundRouteAppAccessResult : IInboundRouteAppAccess
        {
            public int? AccessId { get; set; }
            public int AppId { get; set; }
            public int RouteId { get; set; }
            public string AppName { get; set; }
            public bool IsAccessGranted { get; set; }
            public bool IsAppDisabled { get; set; }
        }
    }

     
}
