﻿using AIT.Database;
using AIT.IOC;
using AIT.IOC.Factories;
using AIT.Mediator.Common.Outbound.Abstractions;
using AIT.Mediator.Database.Inbound;
using AIT.Mediator.Database.Outbound;
using AIT.Mediator.Database.PDF;
using Microsoft.Extensions.DependencyInjection;

namespace AIT.Mediator.Database;

public static class Startup
{
    public static void AddDatabase(this IServiceCollection services)
    {
        services.AddConfigurationTransient<IDatabaseConfiguration, DatabaseConfiguration>("Database");

        services.AddTransient<IDatabaseOptions, DatabaseOptions>();

        services.AddOdbc(
            factoryScope: Scope.Transient,
            procedureAssemblies: typeof(Startup).Assembly
            );



        services.AddTransient<IInboundDataSource, InboundDataSource>();
        services.AddTransient<IPdfDataSource, PdfDataSource>();

        services.AddTransient<IOutboundDataSource, OutboundDataSource>();
        services.AddTransient<OutboundRepository>();
    }
}
