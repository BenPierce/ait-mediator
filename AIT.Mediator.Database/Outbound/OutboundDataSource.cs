﻿using AIT.IOC.Factories;
using AIT.Mediator.Common.Outbound.Abstractions;
using AIT.Mediator.Common.Outbound.Data;
using AIT.Mediator.Database.Outbound.Queries;
using System.Data;

namespace AIT.Mediator.Database.Outbound;

class OutboundDataSource : IOutboundDataSource
{
    private readonly IFactory<IDbConnection> _dbConnectionFactory;
    private readonly OutboundRepository _outboundRepository;

    public OutboundDataSource(
        IFactory<IDbConnection> dbConnectionFactory,
        OutboundRepository outboundRepository
        )
    {
        _dbConnectionFactory = dbConnectionFactory;
        _outboundRepository = outboundRepository;
    }

    public async Task<IOutboundRoute> GetRoute(GetOutboundRouteQueryParameter parameter)
    {
        var query = new GetOutboundRouteQuery(parameter);

        using (var connection = _dbConnectionFactory.Build())
        {
            return await _outboundRepository.GetOutboundRoute(
                query: query,
                connection: connection
                );
        }
    }

    public async Task<bool> GetProceed(string proceedSql, VariableQueryParameter parameter)
    {
        var query = new GetProceedQuery(proceedSql, parameter);

        using (var connection = _dbConnectionFactory.Build())
        {
            return await _outboundRepository.GetProceed(
                query: query,
                connection: connection
                );
        }
    }

    public async Task<string> GetRouteUrl(string outboundUrlSql, VariableQueryParameter parameter)
    {
        var query = new GetRouteUrlQuery(outboundUrlSql, parameter);

        using (var connection = _dbConnectionFactory.Build())
        {
            return await _outboundRepository.GetRouteUrl(
                query: query,
                connection: connection
                );
        }
    }

    public async Task<string> GetHeadersSerialized(string outboundHeadersSql, VariableQueryParameter parameter)
    {
        var query = new GetRouteHeadersSerializedQuery(outboundHeadersSql, parameter);

        using (var connection = _dbConnectionFactory.Build())
        {
            return await _outboundRepository.GetRouteHeadersSerialized(
                query: query,
                connection: connection
                );
        }
    }

    public async Task<IDictionary<string, string>> GetHeaderUnserialized(string outboundHeadersSql, VariableQueryParameter parameter)
    {
        var query = new GetRouteHeadersUnserializedQuery(outboundHeadersSql, parameter);

        IDictionary<string, string> headers = null;
        IEnumerable<IDictionary<string, dynamic>> headersCollection = null;

        using (var connection = _dbConnectionFactory.Build())
        {
            headersCollection = await _outboundRepository.GetRouteHeadersUnserialized(
                query: query,
                connection: connection
                );
        }

        if (headersCollection?.Any() == true)
        {
            if (headersCollection.Count() != 1)
            {
                throw new Exception("Retrieved multiple rows when retrieving headers. Only a single row is supported");
            }
            else
            {
                headers = headersCollection.First()?.Select(h => new KeyValuePair<string, string>(h.Key, h.Value?.ToString()))?.ToDictionary(a => a.Key, a => a.Value);
            }
        }

        return headers;
    }

    public async Task<string> GetBodySerialized(string outboundBodySql, VariableQueryParameter parameter)
    {
        var query = new GetRouteBodySerializedQuery(outboundBodySql, parameter);

        using (var connection = _dbConnectionFactory.Build())
        {
            return await _outboundRepository.GetRouteBodySerialized(
                query: query,
                connection: connection
                );
        }
    }

    public async Task<IEnumerable<dynamic>> GetBodyUnserialized(string outboundBodySql, VariableQueryParameter parameter)
    {
        var query = new GetRouteBodyUnserializedQuery(outboundBodySql, parameter);

        using (var connection = _dbConnectionFactory.Build())
        {
            return await _outboundRepository.GetRouteBodyUnserialized(
                query: query,
                connection: connection
                );
        }
    }

    public async Task ExecuteRouteResponse(
        string query,
        IDictionary<string, object> parameters
        )
    {
        using (var connection = _dbConnectionFactory.Build())
        {
            await _outboundRepository.ExecuteRouteResponse(
                query: query,
                parameters: parameters,
                connection: connection
                );
        }
    }

    public async Task<IEnumerable<IOutboundRoute>> GetOutboundRoutes()
    {
        var query = new GetOutboundRoutesQuery();

        using (var connection = _dbConnectionFactory.Build())
        {
            return await _outboundRepository.GetOutboundRoutes(
                query: query,
                connection: connection
                );
        }
    }

    public async Task<IOutboundRoute> UpsertOutboundRoute(UpsertOutboundRouteQueryParameter parameter)
    {
        var query = new UpsertOutboundRouteQuery(parameter);

        using (var connection = _dbConnectionFactory.Build())
        {
            return await _outboundRepository.UpsertOutboundRoute(
                query: query,
                connection: connection
                )
                ?? throw new Exception("SQL executed but no Outbound Route was returned");
        }
    }

    public async Task DeleteOutboundRoute(DeleteOutboundRouteQueryParameter parameter)
    {
        var query = new DeleteOutboundRouteQuery(parameter);

        using (var connection = _dbConnectionFactory.Build())
        {
            await _outboundRepository.DeleteOutboundRoute(
                query: query,
                connection: connection
                );
        }
    }

    
}
