﻿using AIT.Database;
using AIT.Mediator.Database.Outbound.Models;
using AIT.Mediator.Database.Outbound.Queries;
using System.Data;

namespace AIT.Mediator.Database.Outbound;

class OutboundRepository
{
    public async Task<string> GetRouteUrl(
        GetRouteUrlQuery query,
        IDbConnection connection,
        IDbTransaction transaction = null
        )
    {
        return await connection.SelectOneAsync<string>(
            query: query.Query,
            parameters: query.Parameters,
            isStoredProc: query.IsStoredProc,
            transaction: transaction
            );
    }

    public async Task<bool> GetProceed(
        GetProceedQuery query,
        IDbConnection connection,
        IDbTransaction transaction = null
        )
    {
        return await connection.SelectOneAsync<bool>(
            query: query.Query,
            parameters: query.Parameters,
            isStoredProc: query.IsStoredProc,
            transaction: transaction
            );
    }

    public async Task<string> GetRouteHeadersSerialized(
        GetRouteHeadersSerializedQuery query,
        IDbConnection connection,
        IDbTransaction transaction = null
        )
    {
        return await connection.SelectOneAsync<string>(
            query: query.Query,
            parameters: query.Parameters,
            isStoredProc: query.IsStoredProc,
            transaction: transaction
            );
    }

    public async Task<IEnumerable<IDictionary<string,dynamic>>> GetRouteHeadersUnserialized(
        GetRouteHeadersUnserializedQuery query,
        IDbConnection connection,
        IDbTransaction transaction = null
        )
    {
        return await connection.SelectDictionaryAsync(
            query: query.Query,
            parameters: query.Parameters,
            isStoredProc: query.IsStoredProc,
            transaction: transaction
            );
    }

    public async Task<string> GetRouteBodySerialized(
        GetRouteBodySerializedQuery query,
        IDbConnection connection,
        IDbTransaction transaction = null
        )
    {
        return await connection.SelectOneAsync<string>(
            query: query.Query,
            parameters: query.Parameters,
            isStoredProc: query.IsStoredProc,
            transaction: transaction
            );
    }

    public async Task<IEnumerable<dynamic>> GetRouteBodyUnserialized(
        GetRouteBodyUnserializedQuery query,
        IDbConnection connection,
        IDbTransaction transaction = null
        )
    {
        return await connection.SelectManyAsync<dynamic>(
            query: query.Query,
            parameters: query.Parameters,
            isStoredProc: query.IsStoredProc,
            transaction: transaction
            );
    }

    public async Task ExecuteRouteResponse(
        string query,
        IDictionary<string, object> parameters,
        IDbConnection connection,
        IDbTransaction transaction = null
        )
    {
        await connection.ExecuteAsync(
            query: query,
            parameters: parameters,
            isStoredProc: false,
            transaction: transaction
            );
    }

    public async Task<OutboundRouteDM> GetOutboundRoute(
       GetOutboundRouteQuery query,
       IDbConnection connection,
       IDbTransaction transaction = null
       )
    {
        return await connection.SelectOneAsync<OutboundRouteDM>(
            query: query.Query,
            parameters: query.Parameters,
            isStoredProc: query.IsStoredProc,
            transaction: transaction
            );
    }

    public async Task<IEnumerable<OutboundRouteDM>> GetOutboundRoutes(
       GetOutboundRoutesQuery query,
       IDbConnection connection,
       IDbTransaction transaction = null
       )
    {
        return await connection.SelectManyAsync<OutboundRouteDM>(
            query: query.Query,
            parameters: query.Parameters,
            isStoredProc: query.IsStoredProc,
            transaction: transaction
            );
    }


    public async Task<OutboundRouteDM> UpsertOutboundRoute(
        UpsertOutboundRouteQuery query,
        IDbConnection connection,
        IDbTransaction transaction = null
        )
    {
        return await connection.SelectOneAsync<OutboundRouteDM>(
            query: query.Query,
            parameters: query.Parameters,
            isStoredProc: query.IsStoredProc,
            transaction: transaction
            );
    }

    public async Task DeleteOutboundRoute(
        DeleteOutboundRouteQuery query,
        IDbConnection connection,
        IDbTransaction transaction = null
        )
    {
        await connection.ExecuteAsync(
            query: query.Query,
            parameters: query.Parameters,
            isStoredProc: query.IsStoredProc,
            transaction: transaction
            );
    }
}
