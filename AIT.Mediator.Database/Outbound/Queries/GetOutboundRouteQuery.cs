﻿using AIT.Mediator.Common.Database;
using AIT.Mediator.Common.Outbound.Data;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Outbound.Queries;

class GetOutboundRouteQuery : IQueryBase
{
    public string Query => @"administrator.usp_Mediator_Outbound_GetRoute";
    public bool IsStoredProc => true;
    public QueryParameterBase Parameters { get; private set; }

    public GetOutboundRouteQuery(GetOutboundRouteQueryParameter parameter)
    {
        Parameters = parameter;
    }
}
