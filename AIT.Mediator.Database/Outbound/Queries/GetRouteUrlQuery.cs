﻿using AIT.Mediator.Common.Database;
using AIT.Mediator.Common.Outbound.Data;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Outbound.Queries;

class GetRouteUrlQuery : IQueryBase
{
    public string Query { get; private set; }
    public bool IsStoredProc => false;
    public QueryParameterBase Parameters { get; set; }

    public GetRouteUrlQuery(string outboundUrlSql, VariableQueryParameter parameters)
    {
        if (string.IsNullOrWhiteSpace(outboundUrlSql)) throw new ArgumentNullException(nameof(outboundUrlSql));

        Query = outboundUrlSql;
        Parameters = parameters;
    }
}
