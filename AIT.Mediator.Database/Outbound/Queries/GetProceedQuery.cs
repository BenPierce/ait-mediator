﻿using AIT.Mediator.Common.Database;
using AIT.Mediator.Common.Outbound.Data;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Outbound.Queries;

class GetProceedQuery : IQueryBase
{
    public string Query { get; private set; }
    public bool IsStoredProc => false;
    public QueryParameterBase Parameters { get; set; }

    public GetProceedQuery(string proceedSql, VariableQueryParameter parameters)
    {
        if (string.IsNullOrWhiteSpace(proceedSql)) throw new ArgumentNullException(nameof(proceedSql));

        Query = proceedSql;
        Parameters = parameters;
    }
}
