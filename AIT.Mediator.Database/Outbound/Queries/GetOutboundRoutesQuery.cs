﻿using AIT.Mediator.Common.Database;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Outbound.Queries;

class GetOutboundRoutesQuery : IQueryBase
{
    public string Query => @"administrator.usp_Mediator_Outbound_GetRoutes";
    public bool IsStoredProc => true;
    public QueryParameterBase Parameters => null;

}
