﻿using AIT.Mediator.Common.Database;
using AIT.Mediator.Common.Outbound.Data;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Outbound.Queries;

class GetRouteBodyUnserializedQuery : IQueryBase
{
    public string Query { get; private set; }
    public bool IsStoredProc => false;
    public QueryParameterBase Parameters { get; set; }

    public GetRouteBodyUnserializedQuery(string outboundRouteBodySql, VariableQueryParameter parameter)
    {
        if (string.IsNullOrWhiteSpace(outboundRouteBodySql)) throw new ArgumentNullException(nameof(outboundRouteBodySql));

        Query = outboundRouteBodySql;
        Parameters = parameter;
    }
}
