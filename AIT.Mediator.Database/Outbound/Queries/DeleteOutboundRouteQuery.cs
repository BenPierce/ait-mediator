﻿using AIT.Mediator.Common.Database;
using AIT.Mediator.Common.Outbound.Data;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Outbound.Queries;

class DeleteOutboundRouteQuery : IQueryBase
{
    public string Query => @"administrator.usp_Mediator_Outbound_DeleteRoute";
    public bool IsStoredProc => true;
    public QueryParameterBase Parameters { get; set; }

    public DeleteOutboundRouteQuery(DeleteOutboundRouteQueryParameter parameter)
    {
        Parameters = parameter;
    }
}
