﻿using AIT.Mediator.Common.Database;
using AIT.Mediator.Common.Outbound.Data;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Outbound.Queries;

class GetRouteHeadersSerializedQuery : IQueryBase
{
    public string Query { get; private set; }
    public bool IsStoredProc => false;
    public QueryParameterBase Parameters { get; set; }

    public GetRouteHeadersSerializedQuery(string outboundRouteHeadersSql, VariableQueryParameter parameter)
    {
        if (string.IsNullOrWhiteSpace(outboundRouteHeadersSql)) throw new ArgumentNullException(nameof(outboundRouteHeadersSql));

        Query = outboundRouteHeadersSql;
        Parameters = parameter;
    }
}
