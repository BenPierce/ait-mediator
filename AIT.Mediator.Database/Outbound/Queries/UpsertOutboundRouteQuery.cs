﻿using AIT.Mediator.Common.Database;
using AIT.Mediator.Common.Outbound.Data;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.Outbound.Queries;

class UpsertOutboundRouteQuery : IQueryBase
{
    public string Query => @"administrator.usp_Mediator_Outbound_UpsertRoute";
    public bool IsStoredProc => true;
    public QueryParameterBase Parameters { get; set; }

    public UpsertOutboundRouteQuery(UpsertOutboundRouteQueryParameter parameter)
    {
        Parameters = parameter;
    }
}
