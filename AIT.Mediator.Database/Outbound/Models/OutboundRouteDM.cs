﻿using AIT.Mediator.Common.Core.Enums;
using AIT.Mediator.Common.Outbound.Abstractions;

namespace AIT.Mediator.Database.Outbound.Models;

class OutboundRouteDM : IOutboundRoute
{
    public int? RouteId { get; set; }
    public string Name { get; set; }
    public RestType RequestType { get; set; }
    public string RequestUrlSql { get; set; }
    public string RequestHeadersSql { get; set; }
    public bool DeserializeRequestHeaders { get; set; }
    public string RequestBodySql { get; set; }
    public bool SerializeRequestBody { get; set; }
    public string ResponseSql { get; set; }
    public string ResponseErrorSql { get; set; }
    public bool DeserializeResponse { get; set; }
    public string ProceedSql { get; set; }
    public int? Id => RouteId;
}
