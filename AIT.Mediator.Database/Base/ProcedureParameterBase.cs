﻿namespace AIT.Mediator.Database.Base;


public abstract class ProcedureParameterBase
{
    public string Set(
        string value,
        string fieldName,
        bool isNullable = true,
        int? maxLength = null,
        bool isUpperCase = false
        )
    {
        if (string.IsNullOrWhiteSpace(value))
        {
            if (isNullable == false)
            {
                throw new Exception($"Parameter field {fieldName} can not be null");
            }
            else
            {
                return null;
            }
        }
        else
        {
            if (maxLength != null && value.Length > maxLength)
            {
                throw new Exception($"Parameter field {fieldName} can not be more then {maxLength} characters");
            }
            else
            {
                if (isUpperCase) value = value.ToUpper();

                return value?.Trim();
            }
        }
    }

    public int Set(
        int value,
        string fieldName,
        bool isZeroAllowed = true,
        bool isNegativeAllowed = true,
        int? maxValue = null
        )
    {
        if (isZeroAllowed == false && value == 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be zero");
        }
        else if (isNegativeAllowed == false && value < 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be negative");
        }
        else if (maxValue is not null && value > maxValue.Value)
        {
            throw new Exception($"Parameter field {fieldName} can greater then {maxValue}");
        }
        else
        {
            return value;
        }
    }

    public int? Set(
        int? value,
        string fieldName,
        bool isZeroAllowed = true,
        bool isNegativeAllowed = true,
        int? maxValue = null,
        bool isNullable = true
        )
    {
        if (value is null && isNullable != true)
        {
            throw new Exception($"Parameter field {fieldName} can not be null");
        }
        else if (isZeroAllowed == false && value == 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be zero");
        }
        else if (isNegativeAllowed == false && value < 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be negative");
        }
        else if (maxValue is not null && value is not null && value.Value > maxValue.Value)
        {
            throw new Exception($"Parameter field {fieldName} can greater then {maxValue}");
        }
        else
        {
            return value;
        }
    }

    public double Set(
       double value,
       string fieldName,
       bool isZeroAllowed = true,
       bool isNegativeAllowed = true,
       double? maxValue = null
       )
    {
        if (isZeroAllowed == false && value == 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be zero");
        }
        else if (isNegativeAllowed == false && value < 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be negative");
        }
        else if (maxValue is not null && value > maxValue.Value)
        {
            throw new Exception($"Parameter field {fieldName} can greater then {maxValue}");
        }
        else
        {
            return value;
        }
    }

    public double? Set(
        double? value,
        string fieldName,
        bool isZeroAllowed = true,
        bool isNegativeAllowed = true,
        double? maxValue = null,
        bool isNullable = true
        )
    {
        if (value is null && isNullable != true)
        {
            throw new Exception($"Parameter field {fieldName} can not be null");
        }
        else if (isZeroAllowed == false && value == 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be zero");
        }
        else if (isNegativeAllowed == false && value < 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be negative");
        }
        else if (maxValue is not null && value is not null && value.Value > maxValue.Value)
        {
            throw new Exception($"Parameter field {fieldName} can greater then {maxValue}");
        }
        else
        {
            return value;
        }
    }

    public decimal Set(
       decimal value,
       string fieldName,
       bool isZeroAllowed = true,
       bool isNegativeAllowed = true,
       decimal? maxValue = null
       )
    {
        if (isZeroAllowed == false && value == 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be zero");
        }
        else if (isNegativeAllowed == false && value < 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be negative");
        }
        else if (maxValue is not null && value > maxValue.Value)
        {
            throw new Exception($"Parameter field {fieldName} can greater then {maxValue}");
        }
        else
        {
            return value;
        }
    }

    public decimal? Set(
        decimal? value,
        string fieldName,
        bool isZeroAllowed = true,
        bool isNegativeAllowed = true,
        decimal? maxValue = null,
        bool isNullable = true
        )
    {
        if (value is null && isNullable != true)
        {
            throw new Exception($"Parameter field {fieldName} can not be null");
        }
        else if (isZeroAllowed == false && value == 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be zero");
        }
        else if (isNegativeAllowed == false && value < 0)
        {
            throw new Exception($"Parameter field {fieldName} can not be negative");
        }
        else if (maxValue is not null && value is not null && value.Value > maxValue.Value)
        {
            throw new Exception($"Parameter field {fieldName} can greater then {maxValue}");
        }
        else
        {
            return value;
        }
    }

    public static DateTime Set(
        DateTime value,
        string fieldName,
        bool isAllowedDefault = true,
        int? maxDaysAhead = null,
        bool isTimeAllowed = true,
        bool isDateAllowed = true
        )
    {
        if (isAllowedDefault == false && value == default)
        {
            throw new Exception($"Parameter field {fieldName} can not an empty value");
        }
        else if (maxDaysAhead != null && (value.Date - DateTime.Today).TotalDays > (int)maxDaysAhead)
        {
            throw new Exception($"Parameter field {fieldName} must not be more then {maxDaysAhead} days in the future");
        }
        else
        {
            if (isTimeAllowed)
            {
                if (isDateAllowed)
                {
                    return value;
                }
                else
                {
                    return default(DateTime) + value.TimeOfDay;
                }

            }
            else
            {
                return value.Date;
            }

        }
    }

    public static DateTime? Set(
        DateTime? value,
        string fieldName,
        bool isNullable = true,
        bool isAllowedDefault = true,
        int? maxDaysAhead = null,
        bool isTimeAllowed = true,
        bool isDateAllowed = true
        )
    {
        if (value == null)
        {
            if (isNullable == false)
            {
                throw new Exception($"Parameter field {fieldName} can not be null");
            }
            else
            {
                return null;
            }
        }
        else
        {
            return Set((DateTime)value, fieldName, isAllowedDefault, maxDaysAhead, isTimeAllowed, isDateAllowed);
        }
    }

    public static Guid Set(
        Guid value,
        string fieldName,
        bool isAllowedDefault = true
        )
    {
        if (isAllowedDefault == false && value == default)
        {
            throw new Exception($"Parameter field {fieldName} can not an empty value");
        }
        else
        {
            return value;
        }
    }

    public static bool Set(
        bool value,
        string fieldName
        )
    {
        return value;
    }
}
