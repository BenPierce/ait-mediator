﻿using AIT.Database;

namespace AIT.Mediator.Database;

class DatabaseOptions : IDatabaseOptions
{
    public bool ShouldLog { get; set; } = true;
    public string FilePath { get; set; }
    public object Locker { get; set; } = new();
}
