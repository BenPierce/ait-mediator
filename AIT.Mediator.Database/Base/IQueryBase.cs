﻿using AIT.Mediator.Common.Database;

namespace AIT.Mediator.Database.Base;

interface IQueryBase
{
    string Query { get; }
    bool IsStoredProc { get; }
    QueryParameterBase Parameters { get; }
}
