﻿using AIT.Database;

namespace AIT.Mediator.Database;

class DatabaseConfiguration : IDatabaseConfiguration
{
    public int? SqlVersion { get; set; }
    public string AppInfo { get; set; }
    public string AutoStart { get; set; }
    public string AutoStop { get; set; }
    public string CharSet { get; set; }
    public string CommBufferSize { get; set; }
    public string CommLinks { get; set; }
    public string Compress { get; set; }
    public string CompressionThreshold { get; set; }
    public string ConnectionName { get; set; }
    public string ConnectionPool { get; set; }
    public string DatabaseFile { get; set; }
    public string DatabaseKey { get; set; }
    public string DatabaseName { get; set; }
    public string DatabaseSwitches { get; set; }
    public string DataSourceName { get; set; }
    public string DisableMultiRowFetch { get; set; }
    public string Encryption { get; set; }
    public string EngineName { get; set; }
    public string FileDataSourceName { get; set; }
    public string ForceStart { get; set; }
    public string Integrated { get; set; }
    public string Kerberos { get; set; }
    public string Language { get; set; }
    public string LazyClose { get; set; }
    public string LivenessTimeout { get; set; }
    public string LogFile { get; set; }
    public string MatView { get; set; }
    public string NodeType { get; set; }
    public string SybaseEncryptedPassword { get; set; }
    public string AITEncryptedPassword { get; set; }
    public string NewPassword { get; set; }
    public string PrefetchBuffer { get; set; }
    public string PrefetchRows { get; set; }
    public string RetryConnectionTimeout { get; set; }
    public string ServerName { get; set; }
    public string StartLine { get; set; }
    public string Unconditional { get; set; }
    public string IpAddress { get; set; }
    public string PortNumber { get; set; }
    public string Password { get; set; }
    public string Username { get; set; }
}
