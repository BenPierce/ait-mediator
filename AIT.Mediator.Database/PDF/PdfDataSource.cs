﻿using AIT.IOC.Factories;
using AIT.Mediator.Common.PDF.Abstractions;
using AIT.Mediator.Database.PDF.Procedures;
using System.Data;

namespace AIT.Mediator.Database.PDF;

public interface IPdfDataSource
{
    Task<IPdfDocumentSettings> GetPdfDocumentSettings(GetPdfDocumentSettings.Parameter parameter);
}

class PdfDataSource : IPdfDataSource
{
    private readonly IFactory<IDbConnection> _dbConnectionFactory;

    public PdfDataSource(
        IFactory<IDbConnection> dbConnectionFactory
        )
    {
        _dbConnectionFactory = dbConnectionFactory;
    }

    private IDbConnection BuildConnection() => _dbConnectionFactory.Build();

    public async Task<IPdfDocumentSettings> GetPdfDocumentSettings(GetPdfDocumentSettings.Parameter parameter)
    {
        using (var connection = BuildConnection())
        {
            using (var proc = new GetPdfDocumentSettings())
            {
                return await proc.ExecuteAsync(connection, parameter);
            }
        }
    }
}
