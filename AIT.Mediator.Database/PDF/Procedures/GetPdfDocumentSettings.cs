﻿using AIT.Database.Procedures;
using AIT.Mediator.Common.PDF.Abstractions;
using AIT.Mediator.Database.Base;

namespace AIT.Mediator.Database.PDF.Procedures;

public class GetPdfDocumentSettings : Procedure
    .WithParameters<GetPdfDocumentSettings.Parameter>
    .WithSingleResult<GetPdfDocumentSettings.Result>
{
    public override string Name => @"administrator.usp_Mediator_PDF_GetDocumentSettings";

    public class Parameter : ProcedureParameterBase
    {
        public string In_DocumentNo { get; init; }
        public string In_DocumentCode { get; init; }

        public Parameter(
            string documentNo,
            string documentCode
            )
        {
            In_DocumentNo = Set(
                value: documentNo,
                fieldName: nameof(documentNo),
                isNullable: false,
                isUpperCase: false
                );

            In_DocumentCode = Set(
                value: documentCode,
                fieldName: nameof(documentCode),
                isNullable: false,
                maxLength: 3,
                isUpperCase: true
                );

        }
    }

    public class Result : IPdfDocumentSettings
    {
        public string EnvironmentName { get; set; }
        public string OutputFilePath { get; set; }
        public string TemplateFilePath { get; set; }
        public string ParameterTypes { get; set; }
        public string ParameterValues { get; set; }
    }
}
