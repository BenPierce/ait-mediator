﻿using AIT.Database.Listener;
using Microsoft.Extensions.DependencyInjection;

namespace AIT.Mediator.DatabaseListener
{
    public static class Startup
    {
        public static void AddDatabaseListener(this IServiceCollection services)
        {
            services.AddDatabaseListener<DatabaseEventHandlerService>("DatabaseListener");

            services.AddHostedService<ListenerInitializerService>();
        }
    }
}
