﻿using AIT.Database.Listener.Abstractions;
using AIT.IOC.Factories;
using AIT.Mediator.Common.Inbound.PubSub;
using AIT.Mediator.Common.Outbound.Abstractions;
using AIT.PubSub;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AIT.Mediator.DatabaseListener
{
    class DatabaseEventHandlerService : IDatabaseEventHandler
    {
        private readonly ILogger<DatabaseEventHandlerService> _logger;
        private readonly IFactory<IOutboundRequestService> _outboundRequestServiceFactory;
        private readonly IPubSubSingletonService _pubSubSingletonService;

        public DatabaseEventHandlerService(
            ILogger<DatabaseEventHandlerService> logger,
            IFactory<IOutboundRequestService> outboundRequestServiceFactory,
            IPubSubSingletonService pubSubSingletonService
            )
        {
            _logger = logger;
            _outboundRequestServiceFactory = outboundRequestServiceFactory;
            _pubSubSingletonService = pubSubSingletonService;
        }

        public async Task HandleAsync(IDatabaseEvent databaseEvent)
        {
            try
            {
                switch (databaseEvent.TableName.ToUpper())
                {
                    case "OUTBOUND":
                        ProcessOutboundRequest(databaseEvent);
                        break;

                    case "CHANGE":
                        ProcessCacheChange(databaseEvent);
                        break;

                    default:
                        _logger.LogError($"Received database event that could not be mapped");
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error processing database event: {ex.Message}");
            }
        }

        private void ProcessCacheChange(IDatabaseEvent databaseEvent)
        {
            _pubSubSingletonService.Publish(new InboundRouteCacheChangeMessage());
        }

        private void ProcessOutboundRequest(IDatabaseEvent databaseEvent)
        {
            Task.Run(async () =>
            {
                try
                {                    
                    var eventIdentifier = databaseEvent.NewValues?.FirstOrDefault()
                                        ?? throw new Exception("No event identifier provided");
                    
                    var outboundRequestService = _outboundRequestServiceFactory.Build();

                    int eventId;
                    bool isEventId = int.TryParse(databaseEvent.NewValues.FirstOrDefault(), out eventId);

                    if (isEventId)
                    {

                        var parameters = databaseEvent.NewValues.Count() > 1
                                        ? databaseEvent.NewValues.Skip(1).Select(s => s?.ToString()).ToList()
                                        : null;

                        await outboundRequestService.RequestById(eventId, parameters);
                    }
                    else
                    {
                        throw new Exception("Request route id was not numeric");
                    }
                }
                catch (Exception ex)
                {
                    ex.Data.Add(nameof(databaseEvent), databaseEvent);

                    _logger.LogError(ex, $"Failed to process outbound request");
                }
            });
            

        }

    }
}
