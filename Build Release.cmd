set /P VersionNo=Version No (x.x.x): 

echo Zipping SQL Files
"C:\Program Files\7-Zip\7z.exe" a "D:\GIT\AIT.Mediator\Release\AIT.Mediator - %VersionNo% SQL.zip" "D:\GIT\AIT.Mediator\SQL\*"

echo Zipping App Files
"C:\Program Files\7-Zip\7z.exe" a "D:\GIT\AIT.Mediator\Release\AIT.Mediator - %VersionNo% API.zip" "D:\GIT\AIT.Mediator\Publish.API\*"
"C:\Program Files\7-Zip\7z.exe" a "D:\GIT\AIT.Mediator\Release\AIT.Mediator - %VersionNo% Editor.zip" "D:\GIT\AIT.Mediator\Publish.Editor\*"

echo Copying to OneDrive
XCOPY "D:\GIT\AIT.Mediator\Release" "D:\Auto-IT\Auto-IT New Zealand - General\Common\NZDEV\AIT.Mediator\Releases" /Y /D

